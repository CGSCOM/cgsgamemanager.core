﻿namespace CGSGameManager.Core
{
    public static class Config
    {
        #region EAC

        public static class EAC
        {
            public static bool Enabled = true;

            public static bool KickOnViolation;

            public static bool KickOnClientBanned = true;

            public static void RegisterCommands()
            {
                Interface.Core.CommandProcessor.RegisterCommand("eac.enabled", a =>
                {
                    if (a.Length == 0)
                        Enabled = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                Enabled = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                Enabled = false;
                                break;
                        }

                    Interface.Core.LogInfo($"[CommandProcessor] Config Value 'EAC' has been set to {Enabled}");
                }, "Enable or disable the eac processor");

                Interface.Core.CommandProcessor.RegisterCommand("eac.kickviolation", a =>
                {
                    if (a.Length == 0)
                        KickOnViolation = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                KickOnViolation = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                KickOnViolation = false;
                                break;
                        }

                    Interface.Core.LogInfo(
                        $"[CommandProcessor] Config Value 'EAC Kick on Violation' has been set to {KickOnViolation}");
                }, "Enable or disable the kicking of clients when eac violation is triggered");

                Interface.Core.CommandProcessor.RegisterCommand("eac.kickbanned", a =>
                {
                    if (a.Length == 0)
                        KickOnClientBanned = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                KickOnClientBanned = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                KickOnClientBanned = false;
                                break;
                        }

                    Interface.Core.LogInfo(
                        $"[CommandProcessor] Config Value 'EAC Kicked BannedUsers' has been set to {KickOnClientBanned}");
                }, "Enable or disable the kicking of gamebanned users");
            }
        }

        #endregion

        #region Steam

        public static class Steam
        {
            public static bool AllowSteamUsers = true;

            public static bool AllowLumaUsers = true;

            public static bool KickInvalidTokens = true;


            public static void RegisterCommands()
            {
                Interface.Core.CommandProcessor.RegisterCommand("steam.steamusers", a =>
                {
                    if (a.Length == 0)
                        AllowSteamUsers = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                AllowSteamUsers = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                AllowSteamUsers = false;
                                break;
                        }

                    Interface.Core.LogInfo(
                        $"[CommandProcessor] Config Value 'Allow Steam Users' has been set to {AllowSteamUsers}");
                }, "Enable or disable the ability for steam offical clients to join");
                Interface.Core.CommandProcessor.RegisterCommand("steam.lumausers", a =>
                {
                    if (a.Length == 0)
                        AllowLumaUsers = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                AllowLumaUsers = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                AllowLumaUsers = false;
                                break;
                        }

                    Interface.Core.LogInfo(
                        $"[CommandProcessor] Config Value 'Allow Luma Users' has been set to {AllowLumaUsers}");
                }, "Enable or disable the ability for steam non-offical clients to join");
                Interface.Core.CommandProcessor.RegisterCommand("steam.kickinvalidtokens", a =>
                {
                    if (a.Length == 0)
                        KickInvalidTokens = true;
                    else
                        switch (a[0].ToLower())
                        {
                            case "on":
                            case "enabled":
                            case "true":
                            case "enable":
                            case "1":
                            default:
                                KickInvalidTokens = true;
                                break;

                            case "off":
                            case "disable":
                            case "disabled":
                            case "false":
                            case "0":
                                KickInvalidTokens = false;
                                break;
                        }

                    Interface.Core.LogInfo(
                        $"[CommandProcessor] Config Value 'Kick invalid tokens' has been set to {KickInvalidTokens}");
                }, "Enable or disable the ability for non-steam users to join (Not Tested)");
            }
        }

        #endregion
    }
}
