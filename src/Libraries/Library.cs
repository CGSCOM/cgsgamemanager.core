﻿using System;
using System.Threading;
using CGSGameManager.Core.Extensions;

namespace CGSGameManager.Core.Libraries
{
    public abstract class Library
    {
        private string _name;
        protected CGSManager Core => Interface.Core;
        protected ExtensionManager ExtensionManager => Interface.Core?.ExtensionManager;

        internal void InternalUnload()
        {
            OnRemoved();
            _name = null;
        }

        internal void InternalLoad(string name)
        {
            _name = string.IsNullOrEmpty(name) ? GetType().Name : name;
            OnLoaded();
        }

        protected virtual void OnRemoved()
        {
        }

        protected virtual void OnLoaded()
        {
        }

        protected void RunAsyncTask(WaitCallback callback, object state)
        {
            if (callback == null)
                return;

            Core.QueueAsyncAction(callback, state);
        }

        protected void RunAsyncTask(Action callback)
        {
            if (callback == null)
                return;

            Core.QueueAsyncAction(callback);
        }

        #region Logging

        protected void LogInfo(string message)
        {
            Core.LogInfo($"[{_name}] {message}");
        }

        protected void LogWarning(string message)
        {
            Core.LogWarning($"[{_name}] {message}");
        }

        protected void LogError(string message)
        {
            Core.LogError($"[{_name}] {message}");
        }

        protected void LogException(string message, Exception ex)
        {
            Core.LogException($"[{_name}] {message}", ex);
        }

        #endregion
    }
}
