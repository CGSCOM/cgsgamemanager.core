﻿namespace CGSGameManager.Core.Libraries.WebRequest
{
    public enum RequestMethod
    {
        /// <summary>
        ///     GET is used to request data from a specified resource.
        /// </summary>
        Get = 0x0,

        /// <summary>
        ///     POST is used to send data to a server to create/update a resource.
        /// </summary>
        Post = 0x1,

        /// <summary>
        ///     PUT is used to send data to a server to create/update a resource.
        /// </summary>
        Put = 0x2,

        /// <summary>
        ///     HEAD is almost identical to GET, but without the response body.
        /// </summary>
        Head = 0x4,

        /// <summary>
        ///     The DELETE method deletes the specified resource.
        /// </summary>
        Delete = 0x8,

        /// <summary>
        ///     PATCH is used to update a already existing resource
        /// </summary>
        Patch = 0x10
    }
}
