﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace CGSGameManager.Core.Libraries.WebRequest.ContentTypes
{
    public class UrlEncodedFormDiscriptor : RequestWithBodyDiscriptor
    {
        private StringBuilder _builder;

        public UrlEncodedFormDiscriptor() : base("application/www-form-urlencoded")
        {
            _builder = new StringBuilder();
        }

        public UrlEncodedFormDiscriptor(Encoding encoding, DecompressionMethods compression = DecompressionMethods.None)
            : base("application/www-form-urlencoded", compression, encoding)
        {
            _builder = new StringBuilder();
        }

        /// <summary>
        ///     The original un-encoded string
        /// </summary>
        public string OriginalString => _builder.ToString();

        /// <summary>
        ///     A Url encoded version of <see cref="OriginalString" />
        /// </summary>
        public string UrlEncodedString => Uri.EscapeUriString(OriginalString);

        /// <summary>
        ///     A Data encoded version of <see cref="OriginalString" />
        /// </summary>
        public string DataEncodedString => Uri.EscapeDataString(OriginalString);

        private UrlEncodedFormDiscriptor FinalizeStream()
        {
            if (_builder.Length == 0)
                return this;

            var bytes = Encoding.GetBytes(DataEncodedString);
            Write(bytes, 0, bytes.Length);
            Flush();
            return this;
        }

        protected override void DisposeStarted()
        {
            _builder = null;
        }

        public static implicit operator UrlEncodedFormDiscriptor(Dictionary<string, string> formdata)
        {
            if (formdata != null && formdata.Any())
                return new UrlEncodedFormDiscriptor().Append(formdata);

            return null;
        }

        public static implicit operator UrlEncodedFormDiscriptor(Dictionary<string, string[]> formdata)
        {
            if (formdata != null && formdata.Any())
                return new UrlEncodedFormDiscriptor().Append(formdata);

            return null;
        }

        public static implicit operator UrlEncodedFormDiscriptor(NameValueCollection formdata)
        {
            if (formdata != null && formdata.HasKeys())
                return new UrlEncodedFormDiscriptor().Append(formdata);

            return null;
        }

        #region Appending

        /// <summary>
        ///     Write multiple keys with multiple values to the form data
        /// </summary>
        /// <param name="formdata">Collection of Key-Pairs</param>
        /// <param name="finalizeStream">Closes the stream so no more writes can be done</param>
        /// <returns></returns>
        public UrlEncodedFormDiscriptor Append(IEnumerable<KeyValuePair<string, string[]>> formdata,
            bool finalizeStream = true)
        {
            if (formdata == null)
                return this;

            foreach (var keyValuePair in formdata)
            {
                foreach (var value in keyValuePair.Value ?? new string[] {null})
                {
                    Append(keyValuePair.Key, value);
                }
            }

            return finalizeStream ? FinalizeStream() : this;
        }

        /// <summary>
        ///     Write multiple keys with a single value to the form data
        /// </summary>
        /// <param name="formdata">Collection of Key-Pairs</param>
        /// <param name="finalizeStream">Closes the stream so no more writes can be done</param>
        /// <returns></returns>
        public UrlEncodedFormDiscriptor Append(IEnumerable<KeyValuePair<string, string>> formdata,
            bool finalizeStream = true)
        {
            if (formdata == null)
                return this;

            foreach (var keypair in formdata)
            {
                Append(keypair.Key, keypair.Value);
            }

            return finalizeStream ? FinalizeStream() : this;
        }

        /// <inheritdoc>
        ///     <cref>Append(IEnumerable{KeyValuePair{string,string[]}})</cref>
        /// </inheritdoc>
        public UrlEncodedFormDiscriptor Append(NameValueCollection formdata, bool finalizeStream = true)
        {
            if (formdata == null)
                return this;

            for (var i = 0; i < formdata.Count; i++)
            {
                Append(formdata.GetKey(i), formdata.GetValues(i));
            }

            return finalizeStream ? FinalizeStream() : this;
        }

        /// <summary>
        ///     Appends a key with multiple values to the form
        /// </summary>
        /// <param name="key">Key to write to</param>
        /// <param name="values">Values being written</param>
        /// <param name="finalizeStream">Closes the stream so no more writes can be done</param>
        /// <returns></returns>
        public UrlEncodedFormDiscriptor Append(string key, string[] values, bool finalizeStream = false)
        {
            if (values == null)
            {
                Append(key, (string) null);
                return this;
            }

            foreach (var val in values)
            {
                Append(key, val);
            }

            return finalizeStream ? FinalizeStream() : this;
        }

        /// <summary>
        ///     Appends a key and value to the form
        /// </summary>
        /// <param name="key">The key to add</param>
        /// <param name="value">The value to add</param>
        /// <param name="finalizeStream">Closes the stream so no more writes can be done</param>
        /// <returns></returns>
        public UrlEncodedFormDiscriptor Append(string key, string value, bool finalizeStream = false)
        {
            if (Disposed)
                throw new ObjectDisposedException(GetType().FullName,
                    "This discriptor has been disposed and can't be written to");

            if (Finalized)
                throw new InvalidOperationException("This discriptor isn't taking any more data");

            if (key.IsNullOrWhiteSpace())
                return this;

            if (_builder.Length != 0)
                _builder.Append('&');

            _builder.AppendFormat("{0}={1}", key, value ?? "null");

            return finalizeStream ? FinalizeStream() : this;
        }

        #endregion
    }
}
