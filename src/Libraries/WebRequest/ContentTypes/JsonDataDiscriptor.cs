﻿using System;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace CGSGameManager.Core.Libraries.WebRequest.ContentTypes
{
    public class JsonDataDiscriptor : RequestWithBodyDiscriptor
    {
        private JToken _baseObject;

        public JsonDataDiscriptor(JToken token, DecompressionMethods compression = DecompressionMethods.None,
            Encoding encoding = null) : base("application/json", compression, encoding)
        {
            _baseObject =
                token ?? throw new ArgumentNullException(nameof(token),
                    "No object was provided for the json discriptor");
            Flush();
        }

        public JsonDataDiscriptor(JArray array, DecompressionMethods compression = DecompressionMethods.None,
            Encoding encoding = null) : this((JToken) array, compression, encoding)
        {
        }

        public JsonDataDiscriptor(JObject obj, DecompressionMethods compression = DecompressionMethods.None,
            Encoding encoding = null) : this((JToken) obj, compression, encoding)
        {
        }

        public JsonDataDiscriptor(object obj, DecompressionMethods compression = DecompressionMethods.None,
            Encoding encoding = null) : this(JToken.FromObject(obj), compression, encoding)
        {
        }

        protected override void DisposeStarted()
        {
            _baseObject = null;
        }
    }
}
