﻿using System;

namespace CGSGameManager.Core.Libraries.WebRequest.ContentTypes
{
    public interface IRequestDiscriptor : IDisposable
    {
        /// <summary>
        ///     Overrides the Request Content-Type Header
        /// </summary>
        string ContentType { get; }

        ///// <summary>
        ///// Sets the Request Content-Length header
        ///// </summary>
        //long ContentLength { get; }

        ///// <summary>
        ///// The Encoding used to encode the data with
        ///// </summary>
        //Encoding Encoding { get; }

        ///// <summary>
        ///// Tells the stream how to compress
        ///// </summary>
        //DecompressionMethods CompressionMethod { get; }

        ///// <summary>
        ///// Gets all bytes from the underlying stream
        ///// </summary>
        ///// <returns>All bytes from request</returns>
        //byte[] GetAllBytes();

        ///// <summary>
        ///// Copys all Data to a specified stream
        ///// </summary>
        ///// <param name="stream">Stream to write to</param>
        ///// <param name="seekOffset">Seek Offset</param>
        ///// <param name="orgin">Origin to seek to</param>
        ///// <returns></returns>
        //bool CopyToStream(Stream stream, int seekOffset, SeekOrigin orgin);

        ///// <summary>
        ///// Manually read the underlying stream
        ///// </summary>
        ///// <param name="buffer">Buffer to write the bytes to</param>
        ///// <param name="offset">Offset to start at</param>
        ///// <param name="length">Max amount of bytes to write</param>
        ///// <returns>Amount of bytes read</returns>
        //int Read(byte[] buffer, int offset, int length);
    }
}
