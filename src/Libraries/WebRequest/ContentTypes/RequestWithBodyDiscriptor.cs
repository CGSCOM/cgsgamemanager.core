﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

namespace CGSGameManager.Core.Libraries.WebRequest.ContentTypes
{
    public abstract class RequestWithBodyDiscriptor : IRequestDiscriptor
    {
        /// <summary>
        ///     Initializes a new Stream body
        /// </summary>
        /// <param name="baseStream">Base stream for the content</param>
        /// <param name="contentType">Content-Type Header</param>
        /// <param name="compression">Compression method</param>
        /// <param name="encoding">Encoding to be used</param>
        protected RequestWithBodyDiscriptor(string contentType,
            DecompressionMethods compression = DecompressionMethods.None, Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;

            UnderlyingStream = new MemoryStream();

            Compression = compression;
            Encoding = encoding;

            ContentType = $"{contentType.ToLowerInvariant()}; charset={Encoding.WebName}";
        }

        /// <summary>
        ///     The stream that is written to
        /// </summary>
        private MemoryStream UnderlyingStream { get; }

        /// <inheritdoc cref="MemoryStream.Length" />
        public long StreamLength => UnderlyingStream.Length;

        /// <summary>
        ///     The charset used when writting to the stream
        /// </summary>
        public Encoding Encoding { get; }

        /// <summary>
        ///     The compression method used for this stream
        /// </summary>
        public DecompressionMethods Compression { get; }

        /// <summary>
        ///     <para>True if the stream has been finalized</para>
        ///     Content will no longer we writeable after
        /// </summary>
        public bool Finalized { get; private set; }

        /// <summary>
        ///     This request has been disposed
        /// </summary>
        public bool Disposed { get; private set; }

        /// <inheritdoc />
        public string ContentType { get; }

        /// <inheritdoc cref="Stream.Dispose()" />
        public void Dispose()
        {
            if (Disposed)
                return;
            Disposed = true;
            Finalized = true;
            UnderlyingStream.Dispose();
        }

        /// <inheritdoc cref="Stream.Write" />
        protected void Write(byte[] buffer, int offset, int length)
        {
            if (Disposed)
                throw new ObjectDisposedException(GetType().FullName,
                    "Unable to write to stream as this Discriptor has been disposed");

            if (Finalized)
                throw new InvalidOperationException(
                    $"Unable to write to {GetType().FullName} as the Discriptor has been finalized");

            UnderlyingStream.Write(buffer, offset, length);
        }

        /// <inheritdoc cref="MemoryStream.ToArray" />
        public byte[] ToArray()
        {
            if (Disposed)
                throw new ObjectDisposedException(GetType().FullName, "This discriptor has already been disposed");

            if (!Finalized)
                throw new InvalidOperationException("Stream is not finalized yet");

            return UnderlyingStream.ToArray();
        }

        /// <summary>
        ///     Write this data to another stream
        /// </summary>
        /// <param name="stream">Input stream to write too</param>
        public void CopyToStream(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException(nameof(stream), "Need a input stream to write to");

            var buffer = ToArray();

            stream.Write(buffer, 0, buffer.Length);
        }

        /// <inheritdoc cref="Stream.Flush" />
        public void Flush()
        {
            if (Disposed)
                throw new ObjectDisposedException(GetType().FullName,
                    "The underlying stream has already been disposed");

            if (Finalized)
                return;

            if (Compression != DecompressionMethods.None)
                switch (Compression)
                {
                    case DecompressionMethods.GZip:
                        var buffer = UnderlyingStream.ToArray();
                        UnderlyingStream.SetLength(0);
                        using (var gzip = new GZipStream(UnderlyingStream, CompressionMode.Compress, true))
                        {
                            gzip.Write(buffer, 0, buffer.Length);
                        }

                        break;

                    case DecompressionMethods.Deflate:
                        buffer = UnderlyingStream.ToArray();
                        UnderlyingStream.SetLength(0);
                        using (var deflate = new DeflateStream(UnderlyingStream, CompressionMode.Compress, true))
                        {
                            deflate.Write(buffer, 0, buffer.Length);
                        }

                        break;
                }

            Finalized = true;
        }

        /// <summary>
        ///     Starts the disposal of the derived class
        /// </summary>
        protected virtual void DisposeStarted()
        {
        }
    }
}
