﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Newtonsoft.Json;

namespace CGSGameManager.Core.Libraries.WebRequest
{
    public sealed class Response
    {
        internal Response(HttpWebResponse response)
        {
            try
            {
                ContentLength = response.ContentLength;
                ContentType = response.ContentType;

                ResponseHeaders = new Dictionary<string, string>();

                if (response.Headers != null)
                    for (var i = 0; i < response.Headers.Count; i++)
                        ResponseHeaders[response.Headers.GetKey(i).ToLower()] =
                            string.Join(";", response.Headers.GetValues(i)).ToLower();

                if (response.Method.ToLower() == "head") return;

                var sw = new MemoryStream();

                using (var rs = response.GetResponseStream())
                {
                    var datacache = new byte[256];

                    var bytes = 0;

                    while ((bytes = rs.Read(datacache, 0, datacache.Length)) > 0)
                    {
                        sw.Write(datacache, 0, bytes);
                        datacache = new byte[256];
                    }
                }

                RawData = sw.ToArray();
                if (ContentLength != RawData.Length) ContentLength = RawData.Length;
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                response?.Close();
            }
        }

        private byte[] RawData { get; set; }

        public Dictionary<string, string> ResponseHeaders { get; private set; }

        public long ContentLength { get; private set; }

        public string ContentType { get; private set; }

        public void Dispose()
        {
            RawData = null;
            ResponseHeaders?.Clear();
            ResponseHeaders = null;
            ContentLength = 0;
            ContentType = null;
        }

        public byte[] ReadAsBytes()
        {
            var copy = new byte[RawData.Length];
            Array.Copy(RawData, copy, RawData.Length);
            return copy;
        }

        public string ReadAsString()
        {
            if (RawData == null || RawData?.Length == 0) return null;

            return Encoding.ASCII.GetString(RawData);
        }

        public T ReadAsObject<T>()
        {
            if (ResponseHeaders == null) return default(T);
            if (!ResponseHeaders.ContainsKey("content-type")) return default(T);
            var contentType = ResponseHeaders["content-type"];

            if (typeof(T) == typeof(byte[])) return (T) Convert.ChangeType(ReadAsBytes(), typeof(T));
            if (typeof(T) == typeof(string)) return (T) Convert.ChangeType(ReadAsString(), typeof(string));
            if (typeof(MemoryStream).IsAssignableFrom(typeof(T)))
                return (T) Convert.ChangeType(new MemoryStream(ReadAsBytes()), typeof(Stream));

            if (contentType.Contains("application/json")) return JsonConvert.DeserializeObject<T>(ReadAsString());
            if ((contentType.Contains("text/plain") || contentType.Contains("text/richtext") ||
                 contentType.Contains("text/enriched")) &&
                typeof(T).IsValueType) return (T) Convert.ChangeType(ReadAsString(), typeof(T));

            if (typeof(T).GetCustomAttributes(typeof(SerializableAttribute), false).FirstOrDefault() == null)
                throw new InvalidOperationException(
                    $"{typeof(T).FullName} doesn't inherit the SerializableAttribute");

            if (RawData == null || RawData?.Length == 0) return default(T);

            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream(RawData))
            {
                var response = bf.Deserialize(ms);
                return (T) response;
            }
        }
    }
}
