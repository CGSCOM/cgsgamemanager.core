﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CGSGameManager.Core.Libraries.WebRequest.ContentTypes;

namespace CGSGameManager.Core.Libraries.WebRequest
{
    public delegate void WebRequestCallback(int code, Response response, Exception ex = null);

    public sealed class Request
    {
        private readonly WebRequestSettings _requestSettings;

        public Request(IRequestSettings settings = null)
        {
            _requestSettings = settings != null ? new WebRequestSettings(settings) : new WebRequestSettings();
            var address = BaseAddress;

            if (address != null && !address.IsAbsoluteUri)
                throw new ArgumentException("Tried to create a Request with a non-absolute address");

            lock (ServicePointSyncRoot)
            {
                ServicePointManager.ServerCertificateValidationCallback += CertCallback;
            }
        }

        public void BeginRequest(string url, RequestMethod method, WebRequestCallback callback,
            RequestWithBodyDiscriptor data = null)
        {
            if (callback == null)
                throw new ArgumentNullException(nameof(callback), "Callback is required to send requests");


            HttpWebRequest request;
            if (data is UrlEncodedFormDiscriptor urlEncoded &&
                (method == RequestMethod.Get || method == RequestMethod.Head))
            {
                url += $"?{urlEncoded.UrlEncodedString}";
                data.Dispose();
                data = null;

                request = CreateRequest(url);
                request.Method = method.ToString().ToUpperInvariant();

                try
                {
                    request.BeginGetResponse(r => ProcessResponse(r, request, callback), request);
                }
                catch (Exception e)
                {
                    Interface.Core.LogException(
                        $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to get response", e);
                    callback?.Invoke((int) HttpStatusCode.BadRequest, null, e);
                }

                return;
            }

            request = CreateRequest(url);
            request.Method = method.ToString().ToUpperInvariant();
            if (data != null)
            {
                request.ContentType = data.ContentType;

                switch (data.Compression)
                {
                    case DecompressionMethods.GZip:
                        request.SendChunked = true;
                        request.TransferEncoding = "gzip";
                        break;

                    case DecompressionMethods.Deflate:
                        request.SendChunked = true;
                        request.TransferEncoding = "deflate";
                        break;
                    default:
                        break;
                }

                if (!data.Finalized && !data.Disposed)
                    data.Flush();
                request.ContentLength = data.StreamLength;

                try
                {
                    request.BeginGetRequestStream(r => ProcessData(r, request, callback, data), request);
                }
                catch (Exception e)
                {
                    Interface.Core.LogException(
                        $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to upload data", e);
                    data.Dispose();
                    callback.Invoke((int) HttpStatusCode.BadRequest, null, e);
                }

                return;
            }

            try
            {
                request.BeginGetResponse(r => ProcessResponse(r, request, callback), request);
            }
            catch (Exception e)
            {
                Interface.Core.LogException(
                    $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to get response", e);
                callback?.Invoke((int) HttpStatusCode.BadRequest, null, e);
            }
        }

        private void ProcessData(IAsyncResult result, HttpWebRequest request, WebRequestCallback callback,
            RequestWithBodyDiscriptor dataDiscriptor)
        {
            if (request == null || dataDiscriptor == null)
                return;

            try
            {
                using (dataDiscriptor)
                using (var stream = request.EndGetRequestStream(result))
                {
                    dataDiscriptor.CopyToStream(stream);
                    stream.Close();
                    stream.Dispose();
                    dataDiscriptor.Dispose();
                }

                request.BeginGetResponse(r => ProcessResponse(r, request, callback), request);
            }
            catch (Exception e)
            {
                Interface.Core.LogException(
                    $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to upload data", e);
                dataDiscriptor.Dispose();
                callback?.Invoke((int) HttpStatusCode.BadRequest, null, e);
            }
        }

        private void ProcessResponse(IAsyncResult result, HttpWebRequest request, WebRequestCallback callback)
        {
            Response resp;
            try
            {
                var response = (HttpWebResponse) request.EndGetResponse(result);
                resp = new Response(response);
                callback.Invoke((int) response.StatusCode, resp);
            }
            catch (WebException we)
            {
                if (we.Response == null) throw;

                try
                {
                    var response = (HttpWebResponse) we.Response;
                    resp = new Response(response);
                    callback.Invoke((int) response.StatusCode, resp, we);
                }
                catch (Exception e)
                {
                    Interface.Core.LogException(
                        $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to process response", e);
                }
            }
            catch (Exception e)
            {
                Interface.Core.LogException(
                    $"[WebRequest({request.Method})->{request.Address.AbsoluteUri}] failed to process response", e);
                callback.Invoke((int) HttpStatusCode.BadRequest, null, e);
            }
        }

        /// <summary>
        ///     Modifies this requests settings
        /// </summary>
        /// <param name="onChangeSettings"></param>
        public void ModifySettings(Action<IRequestSettings> onChangeSettings)
        {
            lock (_requestSettings)
            {
                onChangeSettings?.Invoke(_requestSettings);

                if (_requestSettings.BaseAddress == null || _requestSettings.BaseAddress.IsAbsoluteUri) return;
                _requestSettings.BaseAddress = null;
            }

            throw new ArgumentException("The BaseAddress Setting is not of Absolute Kind",
                nameof(_requestSettings.BaseAddress));
        }

        private HttpWebRequest CreateRequest(Uri requestingAddress)
        {
            var baseAddress = BaseAddress;

            if (requestingAddress == null && baseAddress == null)
                throw new ArgumentNullException(nameof(requestingAddress),
                    "Attempted to create a request without a valid Url");

            var request = default(HttpWebRequest);

            if (baseAddress != null)
            {
                if (requestingAddress != null && requestingAddress.IsAbsoluteUri)
                    request = (HttpWebRequest) System.Net.WebRequest.Create(requestingAddress);
                else if
                    (requestingAddress != null)
                    request = (HttpWebRequest) System.Net.WebRequest.Create(new Uri(baseAddress, requestingAddress));
                else
                    request = (HttpWebRequest) System.Net.WebRequest.Create(baseAddress);
            }
            else
            {
                if (!requestingAddress.IsAbsoluteUri)
                    throw new ArgumentException(
                        "Failed to create request with a relative uri because BaseAddress is null",
                        nameof(requestingAddress));
                request = (HttpWebRequest) System.Net.WebRequest.Create(requestingAddress);
            }

            var contentType = ContentType;

            if (!contentType.IsNullOrWhiteSpace())
                request.ContentType = contentType;

            var userAgent = UserAgent;
            if (!userAgent.IsNullOrWhiteSpace())
                request.UserAgent = userAgent;

            lock (_requestSettings)
            {
                if (_requestSettings.MaxRedirects > 0)
                {
                    request.AllowAutoRedirect = true;
                    request.MaximumAutomaticRedirections = _requestSettings.MaxRedirects;
                }
                else
                {
                    request.AllowAutoRedirect = false;
                }

                request.AutomaticDecompression = _requestSettings.AutoDecompression;

                foreach (var header in _requestSettings.DefaultRequestHeaders)
                    AddHeaderValue(request, header.Key, header.Value);
            }

            return request;
        }

        private HttpWebRequest CreateRequest(string requestingAddress)
        {
            return Uri.TryCreate(requestingAddress, UriKind.RelativeOrAbsolute, out var uri)
                ? CreateRequest(uri)
                : CreateRequest((Uri) null);
        }

        #region Information

        /// <summary>
        ///     <para>The base address used to build endpoint urls</para>
        ///     To Change this <see cref="ModifySettings" />
        /// </summary>
        public Uri BaseAddress
        {
            get
            {
                lock (_requestSettings)
                {
                    return _requestSettings.BaseAddress;
                }
            }
        }

        /// <summary>
        ///     <para>The base address used to build endpoint urls</para>
        ///     To Change this <see cref="ModifySettings" />
        /// </summary>
        public string BaseAddressString => BaseAddress?.AbsoluteUri;

        /// <summary>
        ///     <para>Returns the content-type of this Request</para>
        ///     To Change this <see cref="ModifySettings" />
        /// </summary>
        public string ContentType
        {
            get
            {
                var sb = new StringBuilder();
                lock (_requestSettings)
                {
                    if (!_requestSettings.ContentType.IsNullOrWhiteSpace())
                        sb.Append(_requestSettings.ContentType.ToLowerInvariant());

                    if (_requestSettings.Encoding != null)
                        sb.AppendFormat("; charset={0}", _requestSettings.Encoding.WebName);
                }

                return sb.ToString().TrimStart(';', ' ');
            }
        }

        /// <summary>
        ///     <para>Returns the UserAgent Header</para>
        ///     To Change this <see cref="ModifySettings" />
        /// </summary>
        public string UserAgent
        {
            get
            {
                lock (_requestSettings)
                {
                    return _requestSettings.UserAgent;
                }
            }
        }

        #endregion

        #region Static Helpers

        private static readonly string[] BlacklistedHeaders;

        private static readonly IDictionary<string, PropertyInfo> PrivateHeaderProperties;

        static Request()
        {
            CertCallback = FixHttpValidation;

            BlacklistedHeaders = new[]
            {
                "Accept", "Connection", "Content-Length", "Content-Type",
                "Date", "Expect", "Host", "If-Modified-Since",
                "Keep-Alive", "Proxy-Connection", "Range", "Referer",
                "Transfer-Encoding", "User-Agent"
            };

            PrivateHeaderProperties = new Dictionary<string, PropertyInfo>(BlacklistedHeaders.Length);

            var type = typeof(HttpWebRequest);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in BlacklistedHeaders)
            {
                var nametrim = property.Replace("-", string.Empty);
                var p = properties.FirstOrDefault(a => a.Name.Equals(nametrim));

                if (p != null)
                    PrivateHeaderProperties.Add(property, p);
            }
        }

        private static void AddHeaderValue(HttpWebRequest request, string headername, IEnumerable<string> values)
        {
            if (request == null)
                return;

            if (headername.IsNullOrWhiteSpace())
                return;

            var value = string.Join("; ", values.ToArray());

            if (!BlacklistedHeaders.Contains(headername))
            {
                if (value.IsNullOrWhiteSpace())
                {
                    request.Headers.Remove(headername);
                    return;
                }

                request.Headers.Set(headername, value);
                return;
            }

            if (!PrivateHeaderProperties.TryGetValue(headername, out var property))
                return;

            var propType = property.PropertyType;

            if (propType == typeof(string))
            {
                property.SetValue(request, value, null);
            }
            else if (propType == typeof(long))
            {
                var firstvalue = values.FirstOrDefault();

                if (firstvalue.IsNullOrWhiteSpace())
                    return;

                if (long.TryParse(firstvalue, out var lg))
                    property.SetValue(request, lg, null);
            }
            else if
                (propType == typeof(DateTime))
            {
                var firstvalue = values.FirstOrDefault();

                if (firstvalue.IsNullOrWhiteSpace())
                    return;

                if (DateTime.TryParse(firstvalue, out var dt))
                    property.SetValue(request, dt, null);
            }
            else if
                (propType == typeof(bool))
            {
                var firstvalue = values.FirstOrDefault();

                if (firstvalue.IsNullOrWhiteSpace())
                    return;

                if (bool.TryParse(firstvalue, out var b))
                    property.SetValue(request, b, null);
            }
        }

        private static readonly RemoteCertificateValidationCallback CertCallback;

        private static bool FixHttpValidation(object sender, X509Certificate cert, X509Chain chain,
            SslPolicyErrors sslerrors)
        {
            var flag = true;
            if (sslerrors == SslPolicyErrors.None) return true;

            foreach (var t in chain.ChainStatus)
                if (t.Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;

                    if (chain.Build((X509Certificate2) cert)) continue;
                    flag = false;
                    break;
                }

            return flag;
        }

        private static readonly object ServicePointSyncRoot = new object();

        #endregion
    }
}
