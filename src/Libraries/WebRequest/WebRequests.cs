﻿using CGSGameManager.Core.Libraries.WebRequest.ContentTypes;

namespace CGSGameManager.Core.Libraries.WebRequest
{
    public sealed class WebRequests : Library
    {
        static WebRequests()
        {
            Settings = new WebRequestSettings();
        }

        public static IRequestSettings Settings { get; }

        public void Enqueue(string url, RequestWithBodyDiscriptor discriptor, WebRequestCallback callback,
            RequestMethod method)
        {
            var request = CreateRequestClient();

            request.BeginRequest(url, method, callback, discriptor);
        }

        public void EnqueueGet(string url, WebRequestCallback callback)
        {
            Enqueue(url, null, callback, RequestMethod.Get);
        }

        public void EnqueuePost(string url, RequestWithBodyDiscriptor discriptor, WebRequestCallback callback)
        {
            Enqueue(url, discriptor, callback, RequestMethod.Post);
        }

        public void EnqueuePut(string url, RequestWithBodyDiscriptor discriptor, WebRequestCallback callback)
        {
            Enqueue(url, discriptor, callback, RequestMethod.Put);
        }


        public Request CreateRequestClient()
        {
            return new Request(Settings);
        }
    }
}
