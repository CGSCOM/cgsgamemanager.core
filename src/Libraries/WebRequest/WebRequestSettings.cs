﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CGSGameManager.Core.Libraries.WebRequest
{
    public class WebRequestSettings : IRequestSettings
    {
        internal WebRequestSettings()
        {
            Encoding = Encoding.UTF8;
            BaseAddress = null;
            ContentType = "application/x-www-form-urlencoded";
            MaxRedirects = 1;
            UserAgent = BuildUserAgent();
            AutoDecompression = DecompressionMethods.None;
            DefaultRequestHeaders = new Dictionary<string, string[]>();
        }

        internal WebRequestSettings(IRequestSettings baseSettings)
        {
            if (baseSettings == null)
                throw new ArgumentNullException(nameof(baseSettings), "Failed to Copy Settings to WebRequestSettings");

            BaseAddress = baseSettings.BaseAddress;
            Encoding = baseSettings.Encoding;
            ContentType = baseSettings.ContentType;
            UserAgent = baseSettings.UserAgent;
            MaxRedirects = baseSettings.MaxRedirects;
            AutoDecompression = baseSettings.AutoDecompression;
            DefaultRequestHeaders = baseSettings.DefaultRequestHeaders;
        }

        internal WebRequestSettings(WebRequestSettings baseSettings) : this((IRequestSettings) baseSettings)
        {
            // TODO: Add Class Specific Copy
        }

        /// <inheritdoc />
        public Uri BaseAddress { get; set; }

        /// <inheritdoc />
        public Encoding Encoding { get; set; }

        /// <inheritdoc />
        public string ContentType { get; set; }

        /// <inheritdoc />
        public string UserAgent { get; set; }

        /// <inheritdoc />
        public int MaxRedirects { get; set; }

        /// <inheritdoc />
        public DecompressionMethods AutoDecompression { get; set; }

        /// <inheritdoc />
        public IDictionary<string, string[]> DefaultRequestHeaders { get; }

        public static WebRequestSettings Copy(WebRequestSettings settings)
        {
            return new WebRequestSettings(settings);
        }

        public static WebRequestSettings Copy(IRequestSettings settings)
        {
            return new WebRequestSettings(settings);
        }

        private static string BuildUserAgent()
        {
            var sb = new StringBuilder("Mozilla/5.0");

            var osversion = string.Empty;

            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                    osversion = "Windows NT";

                    if (IntPtr.Size == 8)
                        osversion += "; Win64";
                    else
                        osversion += "; Win32";
                    break;


                case PlatformID.MacOSX:
                    osversion = $"Macintosh; {Environment.OSVersion.VersionString}";
                    break;

                case PlatformID.Unix:
                    osversion = "Linux; U";
                    break;

                default:
                    osversion = "Unknown; U";
                    break;
            }

            if (IntPtr.Size == 8)
                osversion += "; x64";
            else
                osversion += "; x86_64";

            sb.AppendFormat(" ({0})", osversion)
                .Append(" AppleWebKit/537.36 (KHTML, like Gecko) CGS/")
                .Append(VersionNumber.CgsCurrent());

            return sb.ToString().Trim();
        }
    }
}
