﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CGSGameManager.Core.Libraries.WebRequest
{
    public interface IRequestSettings
    {
        /// <summary>
        ///     The Requesting Address for this WebRequest
        /// </summary>
        Uri BaseAddress { get; set; }

        /// <summary>
        ///     Sets the Content Encoding for the WebRequest
        /// </summary>
        Encoding Encoding { get; set; }

        /// <summary>
        ///     Sets the Content-Type header for the WebRequest
        /// </summary>
        string ContentType { get; set; }

        /// <summary>
        ///     Sets the UserAgent header for the WebRequest
        /// </summary>
        string UserAgent { get; set; }

        /// <summary>
        ///     Max allowed redirections (Setting this value to Zero disabled redirects)
        /// </summary>
        int MaxRedirects { get; set; }

        /// <summary>
        ///     Response decompression handling
        /// </summary>
        DecompressionMethods AutoDecompression { get; set; }

        /// <summary>
        ///     Headers that are applied with every request
        /// </summary>
        IDictionary<string, string[]> DefaultRequestHeaders { get; }
    }
}
