﻿using System.Collections.Generic;
using System.Linq;
using CGSGameManager.Core.Covalence;

namespace CGSGameManager.Core.Libraries
{
    public sealed class Translations : Library
    {
        private readonly Dictionary<string, Dictionary<string, string>> _languages;

        internal Translations()
        {
            _languages = new Dictionary<string, Dictionary<string, string>>();
        }

        protected override void OnLoaded()
        {
            _languages["en"] = new Dictionary<string, string>();
        }

        protected override void OnRemoved()
        {
            foreach (var dict in _languages.Values) dict?.Clear();

            _languages.Clear();
        }

        /// <summary>
        /// Gets a registered message with a specified key
        /// </summary>
        /// <param name="key">Message Key</param>
        /// <param name="iso">Language override ISO 639</param>
        /// <returns>Language key for specified langauge or defaults to en</returns>
        public string GetMessage(string key, string iso = "en")
        {
            if (key.IsNullOrWhiteSpace())
                return null;

            if (iso.IsNullOrWhiteSpace())
                iso = "en";

            iso = iso.ToLowerInvariant();

            if (!_languages.ContainsKey(iso))
                iso = "en";

            var translations = _languages[iso] ?? (_languages[iso] = new Dictionary<string, string>());

            if (translations.ContainsKey(key)) return translations[key];

            if (iso.Equals("en"))
                return null;

            translations = _languages["en"];

            return !translations.ContainsKey(key) ? null : translations[key];
        }

        /// <summary>
        /// Gets a registered message with a specified key
        /// </summary>
        /// <param name="key">Message Key</param>
        /// <param name="player">player language override</param>
        /// <returns>Language key for specified langauge or defaults to en</returns>
        public string GetMessage(string key, ICgsPlayer player) => GetMessage(key, player?.Language);

        /// <summary>
        /// Registers a message with the translation matrix
        /// </summary>
        /// <param name="key">Key for the stored message</param>
        /// <param name="value">Message</param>
        /// <param name="iso">Language Code ISO 639</param>
        /// <returns>True if message was registered successfully</returns>
        public bool RegisterMessage(string key, string value, string iso = "en")
        {
            if (iso.IsNullOrWhiteSpace())
                iso = "en";

            if (key.IsNullOrWhiteSpace() || value.IsNullOrWhiteSpace())
                return false;

            iso = iso.ToLowerInvariant();

            if (!_languages.TryGetValue(iso, out var translations))
            {
                translations = _languages[iso] = new Dictionary<string, string>();
            }

            translations[key] = value;
            return true;
        }

        /// <summary>
        /// Registers a message with the translation matrix
        /// </summary>
        /// <param name="messages">Message Key/Values</param>
        /// <param name="iso">Language Code ISO 639</param>
        /// <returns>Number of successful registered messages</returns>
        public int RegisterMessage(IDictionary<string, string> messages, string iso = "en")
        {
            if (messages == null || messages.Count == 0)
                return 0;

            return messages.Count(message => RegisterMessage(message.Key, message.Value, iso));
        }

        /// <summary>
        /// Registers a message with the translation matrix
        /// </summary>
        /// <param name="key">Message Key</param>
        /// <param name="translations">Collection of ISO 639 Lang Code and message translation</param>
        /// <returns>Number of successful registered messages</returns>
        public int RegisterMessage(string key, IDictionary<string, string> translations)
        {
            if (key.IsNullOrWhiteSpace() || translations == null)
                return 0;

            return (from translation in translations
                where !translation.Key.IsNullOrWhiteSpace() &&
                      !translation.Value.IsNullOrWhiteSpace() let
                    iso = translation.Key.ToLowerInvariant() where
                    RegisterMessage(key, translation.Value, iso) select
                    translation)
                    .Count();
        }
    }
}
