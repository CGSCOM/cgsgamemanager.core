﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CGSGameManager.Core.Libraries
{
    public class GameDetails : Library
    {
        public static GameDetails Instance => Interface.Core.ExtensionManager.GetLibrary<GameDetails>();

        public string Game { get; protected set; }

        public string Version { get; protected set; }

        public string Protocol { get; protected set; }

        protected override void OnLoaded()
        {
            if (DetectGame())
                LogInfo($"Game Detected: {Game} {Protocol} v{Version}");
            else if (!string.IsNullOrEmpty(Game))
                LogWarning($"Game {Game} was detected but was unable to retrieve Version and Protocol");
            else
                LogError("Unable to detect Currently running game");
        }

        protected virtual bool DetectGame()
        {
            var name = GetGameExecutableName();
            if (string.IsNullOrEmpty(name))
                return false;

            switch (name)
            {
                case "rustdedicated.exe":
                    Game = "Rust";
                    var prototype = GetExportedTypeByName("Assembly-CSharp.dll", "Rust.Protocol");
                    var versiontype = GetExportedTypeByName("Facepunch.Unity.dll", "Facepunch.BuildInfo+BuildDesc");
                    var baseversiontype = GetExportedTypeByName("Facepunch.Unity.dll", "Facepunch.BuildInfo");
                    if (prototype != null)
                        Protocol = prototype.GetProperty("printable", BindingFlags.Static | BindingFlags.Public)
                            ?.GetValue(null, null).ToString();

                    if (baseversiontype == null || versiontype == null)
                        return !string.IsNullOrEmpty(Protocol) && !string.IsNullOrEmpty(Version);

                    var buildinfo = baseversiontype.GetProperty("Current", BindingFlags.Static | BindingFlags.Public)?
                        .GetValue(null, null);

                    if (buildinfo == null)
                        return false;

                    var builddesc = buildinfo.GetType()
                        .GetProperty("Build", BindingFlags.Public | BindingFlags.Instance)
                        ?.GetValue(buildinfo, null);

                    Version = versiontype.GetProperty("Number", BindingFlags.Public | BindingFlags.Instance)
                        ?.GetValue(builddesc, null).ToString();

                    return !string.IsNullOrEmpty(Protocol) && !string.IsNullOrEmpty(Version);

                case "rust_server.exe":
                    Game = "Rust Legacy";
                    prototype = GetExportedTypeByName("Assembly-CSharp.dll", "Rust.Defines.Connection");

                    if (prototype == null) return false;

                    Protocol = prototype.GetField("protocol", BindingFlags.Public | BindingFlags.Static)
                        ?.GetValue(null)?.ToString();

                    Version = Protocol;
                    return !string.IsNullOrEmpty(Protocol) && !string.IsNullOrEmpty(Version);

                default:
                    var proc = new FileInfo(Process.GetCurrentProcess().MainModule.FileName);
                    Game = proc.Name.Replace(proc.Extension, string.Empty).Replace("Dedicated", string.Empty);
                    return false;
            }
        }

        protected virtual string GetGameExecutableName()
        {
            var mainDirExelist = Core.RootDirectory.GetFiles("*.exe", SearchOption.TopDirectoryOnly);

            if (!mainDirExelist.Any())
                return null;

            if (mainDirExelist.Length == 1)
                return mainDirExelist[0].Name.ToLower();

            var filter = mainDirExelist.Where(a => a.Name.Contains("Dedicated")).ToList();

            return !filter.Any() ? null : filter[0].Name.ToLower();
        }

        private static Assembly FindAssemblyByName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            var a = AppDomain.CurrentDomain.GetAssemblies()
                .SingleOrDefault(assembly =>
                    !string.IsNullOrEmpty(assembly.Location) && new FileInfo(assembly.Location).Name == name);

            return a;
        }

        private static Type GetExportedTypeByName(Assembly assembly, string name)
        {
            if (assembly == null || string.IsNullOrEmpty(name))
                return null;

            var types = assembly.GetExportedTypes();

            return types.FirstOrDefault(type => type.FullName == name);
        }

        private static Type GetExportedTypeByName(string assembly, string typename)
        {
            if (string.IsNullOrEmpty(assembly) || string.IsNullOrEmpty(typename))
                return null;

            var a = FindAssemblyByName(assembly);

            return a != null ? GetExportedTypeByName(a, typename) : null;
        }
    }
}
