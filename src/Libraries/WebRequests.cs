﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Net;
//using System.Net.Cache;
//using System.Net.Security;
//using System.Reflection;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Security.Cryptography;
//using System.Security.Cryptography.X509Certificates;
//using System.Text;
//using Newtonsoft.Json;

//namespace CGSGameManager.Core.Libraries
//{
//    public class WebRequests : Library
//    {
//        public enum RequestMethod
//        {
//            GET,
//            POST,
//            DELETE,
//            PUT,
//            PATCH,
//            HEAD
//        }

//        private static RemoteCertificateValidationCallback _certCallback;

//        public WebRequests()
//        {
//            if (_certCallback == null) _certCallback = FixHttpValidation;
//            ServicePointManager.ServerCertificateValidationCallback = _certCallback;
//        }

//        private static bool FixHttpValidation(object sender, X509Certificate cert, X509Chain chain,
//            SslPolicyErrors sslerrors)
//        {
//            var flag = true;
//            if (sslerrors != SslPolicyErrors.None)
//                for (var index = 0; index < chain.ChainStatus.Length; ++index)
//                    if (chain.ChainStatus[index].Status != X509ChainStatusFlags.RevocationStatusUnknown)
//                    {
//                        chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
//                        chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
//                        chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
//                        chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;

//                        if (chain.Build((X509Certificate2) cert)) continue;
//                        flag = false;
//                        break;
//                    }

//            return flag;
//        }

//        public CGSWebRequest Create(Uri url, byte[] data, Action<HttpStatusCode, CGSWebResponse> callback,
//            RequestMethod method, Dictionary<object, object> headers = null)
//        {
//            var r = new CGSWebRequest(url, data, callback, method);
//            if (headers != null) r.SetHeaders(headers);
//            return r;
//        }

//        public CGSWebRequest Create(Uri url, string data, Action<HttpStatusCode, CGSWebResponse> callback,
//            RequestMethod method, Dictionary<object, object> headers = null)
//        {
//            var d = string.IsNullOrEmpty(data) ? null : Encoding.UTF8.GetBytes(data);
//            return Create(url, d, callback, method, headers);
//        }

//        public class CGSWebResponse : IDisposable
//        {
//            internal CGSWebResponse(HttpWebResponse response)
//            {
//                try
//                {
//                    ContentLength = response.ContentLength;
//                    ContentType = response.ContentType;

//                    ResponseHeaders = new Dictionary<string, string>();

//                    if (response.Headers != null)
//                        for (var i = 0; i < response.Headers.Count; i++)
//                            ResponseHeaders[response.Headers.GetKey(i).ToLower()] =
//                                string.Join(";", response.Headers.GetValues(i)).ToLower();

//                    if (response.Method.ToLower() == "head") return;

//                    var sw = new MemoryStream();

//                    using (var rs = response.GetResponseStream())
//                    {
//                        var datacache = new byte[256];

//                        var bytes = 0;

//                        while ((bytes = rs.Read(datacache, 0, datacache.Length)) > 0)
//                        {
//                            sw.Write(datacache, 0, bytes);
//                            datacache = new byte[256];
//                        }
//                    }

//                    RawData = sw.ToArray();
//                    if (ContentLength != RawData.Length) ContentLength = RawData.Length;
//                }
//                catch (Exception)
//                {
//                    // ignored
//                }
//                finally
//                {
//                    response?.Close();
//                }
//            }

//            private byte[] RawData { get; set; }

//            public Dictionary<string, string> ResponseHeaders { get; private set; }

//            public long ContentLength { get; private set; }

//            public string ContentType { get; private set; }

//            public void Dispose()
//            {
//                RawData = null;
//                ResponseHeaders?.Clear();
//                ResponseHeaders = null;
//                ContentLength = 0;
//                ContentType = null;
//            }

//            public byte[] ReadAsBytes()
//            {
//                var copy = new byte[RawData.Length];
//                Array.Copy(RawData, copy, RawData.Length);
//                return copy;
//            }

//            public string ReadAsString()
//            {
//                if (RawData == null || RawData?.Length == 0) return null;

//                return Encoding.ASCII.GetString(RawData);
//            }

//            public T ReadAsObject<T>()
//            {
//                if (ResponseHeaders == null) return default(T);
//                if (!ResponseHeaders.ContainsKey("content-type")) return default(T);
//                var contentType = ResponseHeaders["content-type"];

//                if (typeof(T) == typeof(byte[])) return (T) Convert.ChangeType(ReadAsBytes(), typeof(T));
//                if (typeof(T) == typeof(string)) return (T) Convert.ChangeType(ReadAsString(), typeof(string));
//                if (typeof(MemoryStream).IsAssignableFrom(typeof(T)))
//                    return (T) Convert.ChangeType(new MemoryStream(ReadAsBytes()), typeof(Stream));

//                if (contentType.Contains("application/json")) return JsonConvert.DeserializeObject<T>(ReadAsString());
//                if ((contentType.Contains("text/plain") || contentType.Contains("text/richtext") ||
//                     contentType.Contains("text/enriched")) &&
//                    typeof(T).IsValueType) return (T) Convert.ChangeType(ReadAsString(), typeof(T));

//                if (typeof(T).GetCustomAttributes(typeof(SerializableAttribute), false).FirstOrDefault() == null)
//                    throw new InvalidOperationException(
//                        $"{typeof(T).FullName} doesn't inherit the SerializableAttribute");

//                if (RawData == null || RawData?.Length == 0) return default(T);

//                var bf = new BinaryFormatter();
//                using (var ms = new MemoryStream(RawData))
//                {
//                    var response = bf.Deserialize(ms);
//                    return (T) response;
//                }
//            }
//        }

//        public sealed class CGSWebRequest : IDisposable
//        {
//            private readonly Action<HttpStatusCode, CGSWebResponse> _callback;
//            private byte[] _sendData;

//            internal CGSWebRequest(Uri uri, byte[] data, Action<HttpStatusCode, CGSWebResponse> callback, RequestMethod method)
//            {
//                _callback = callback;
//                IsNetworkError = false;
//                IsHttpError = false;
//                _sendData = data;
//                var wr = (HttpWebRequest) System.Net.WebRequest.Create(uri);

//                wr.UserAgent =
//                    $"Mozilla/5.0 ({Environment.OSVersion}; {(IntPtr.Size == 8 ? "x64" : "x86")}) AppleWebKit/537.36 (KHTML, like Gecko) CGS/1.0";
//                wr.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore);
//                wr.AllowAutoRedirect = true;
//                wr.MaximumAutomaticRedirections = 5;
//                wr.Referer = "https://www.crackedgameservers.com";
//                wr.Timeout = (int)TimeSpan.FromSeconds(20).TotalMilliseconds;
//                wr.Method = method.ToString();
//                wr.ProtocolVersion = new Version(1, 1);
//                wr.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
//                wr.ContentType = "application/x-www-form-urlencoded";
//                BaseRequest = wr;
//                Method = method;
//            }

//            public void Dispose()
//            {
//                Response?.Dispose();
//                Response = null;
//                BaseRequest = null;
//                StatusMessage = null;
//                _sendData = null;
//            }

//            public void SetHeaders(Dictionary<object, object> headers)
//            {
//                BaseRequest.SetHeaders(headers);
//            }

//            public void StartRequest()
//            {
//                ProcessRequestData();
//            }

//            private void ProcessRequestData()
//            {
//                if (Method == RequestMethod.GET || Method == RequestMethod.HEAD)
//                {
//                    GetResponse();
//                    return;
//                }

//                if (_sendData == null || _sendData.Length == 0)
//                {
//                    GetResponse();
//                    return;
//                }

//                BaseRequest.ContentLength = _sendData.Length;

//                try
//                {
//                    BaseRequest.BeginGetRequestStream(ProcessRequestStream, BaseRequest);
//                }
//                catch (ProtocolViolationException)
//                {
//                    if (BaseRequest.KeepAlive)
//                    {
//                        StatusMessage = "KeepAlive should be false when sending data";
//                    }
//                    else if (!BaseRequest.AllowWriteStreamBuffering)
//                    {
//                        StatusMessage = "AllowWriteStreamBuffering must be true";
//                    }
//                    else if (BaseRequest.ContentLength == -1)
//                    {
//                        StatusMessage = "Content-Length was not set";
//                    }
//                    IsNetworkError = true;
//                    StatusCode = HttpStatusCode.SeeOther;
//                    ProcessingResponse = false;
//                }
//                catch (Exception e)
//                {
//                    IsNetworkError = true;
//                    StatusCode = HttpStatusCode.SeeOther;
//                    ProcessingResponse = false;
//                    StatusMessage = e.Message;
//                }
//            }

//            public void GetResponse()
//            {
//                try
//                {
//                    ProcessingResponse = true;
//                    BaseRequest.BeginGetResponse(ProcessResponse, BaseRequest);
//                }
//                catch (Exception ex)
//                {
//                    ProcessingResponse = false;
//                    StatusCode = HttpStatusCode.SeeOther;
//                    StatusMessage = ex.Message;
//                }
//            }

//            #region Fields

//            /// <summary>
//            ///     The base request that powers this class
//            /// </summary>
//            public HttpWebRequest BaseRequest { get; private set; }

//            /// <summary>
//            ///     The selected Request Method
//            /// </summary>
//            public RequestMethod Method { get; }

//            /// <summary>
//            ///     Has a network error occured. This will be true if the reqest failed to send
//            /// </summary>
//            public bool IsNetworkError { get; private set; }

//            /// <summary>
//            ///     Was a status code returned other then 200(OK)
//            /// </summary>
//            public bool IsHttpError { get; private set; }

//            /// <summary>
//            ///     The returned statuscode if any
//            /// </summary>
//            public HttpStatusCode StatusCode { get; private set; }

//            /// <summary>
//            ///     The Status message backing the Status Code
//            /// </summary>
//            public string StatusMessage { get; private set; }

//            /// <summary>
//            ///     The Response
//            /// </summary>
//            public CGSWebResponse Response { get; private set; }

//            /// <summary>
//            ///     The request is asyncronously being processed
//            /// </summary>
//            public bool ProcessingRequest { get; private set; }

//            /// <summary>
//            ///     The response is asyncronously being processed
//            /// </summary>
//            public bool ProcessingResponse { get; private set; }

//            #endregion

//            #region Helpers

//            private void ProcessRequestStream(IAsyncResult result)
//            {
//                try
//                {
//                    using (var request = BaseRequest.EndGetRequestStream(result))
//                    {
//                        request.Write(_sendData, 0, _sendData.Length);
//                        var md5 = GetContentMD5();
//                        BaseRequest.Headers.Add(HttpRequestHeader.ContentMd5, md5);
//                    }
//                    GetResponse();
//                }
//                catch (Exception e)
//                {
//                    StatusMessage = $"Unable to open Request Stream - {e.GetType().Name}";
//                    IsNetworkError = true;
//                    StatusCode = HttpStatusCode.SeeOther;
//                }
//                finally
//                {
//                    ProcessingRequest = false;
//                }
//            }

//            private void ProcessResponse(IAsyncResult result)
//            {
//                try
//                {
//                    var webResponse = BaseRequest.EndGetResponse(result);
//                    var r = webResponse as HttpWebResponse;
//                    StatusCode = r.StatusCode;
//                    StatusMessage = r.StatusDescription;
//                    Response = new CGSWebResponse(r);
//                }
//                catch (WebException ex)
//                {
//                    if (ex.Response != null)
//                    {
//                        Response = new CGSWebResponse(ex.Response as HttpWebResponse);
//                        return;
//                    }

//                    StatusMessage = ex.Message;
//                    StatusCode = ((HttpWebResponse) ex.Response).StatusCode;
//                }
//                catch (Exception ex)
//                {
//                    StatusMessage = ex.Message;
//                    StatusCode = HttpStatusCode.SeeOther;
//                }
//                finally
//                {
//                    ProcessingResponse = false;
//                    _callback?.Invoke(StatusCode, Response);
//                }
//            }

//            //public IEnumerable<Cookie> GetAllCookies()
//            //{
//            //    if (BaseRequest.CookieContainer == null) yield break;
//            //    var k = (Hashtable) BaseRequest.CookieContainer.GetType()
//            //        .GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic)
//            //        .GetValue(BaseRequest.CookieContainer);

//            //    foreach (DictionaryEntry element in k)
//            //    {
//            //        var l = (SortedList) element.Value.GetType()
//            //            .GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);

//            //        foreach (var e in l)
//            //        {
//            //            var cl = (CookieCollection) ((DictionaryEntry) e).Value;

//            //            foreach (Cookie fc in cl)
//            //            {
//            //                if (fc.Domain != BaseRequest.Address.Host) break;
//            //                yield return fc;
//            //            }
//            //        }
//            //    }
//            //}

//            private string GetContentMD5()
//            {
//                if (_sendData == null) return null;
//                using (var md5 = MD5.Create())
//                {
//                    return BitConverter.ToString(md5.ComputeHash(_sendData)).Replace("-", string.Empty).ToUpper();
//                }
//            }

//            #endregion
//        }
//    }

//    public static class HttpWebRequestEx
//    {
//        private static readonly string[] BlacklistedHeaders =
//        {
//            "Accept",
//            "Connection",
//            "Content-Length",
//            "Content-Type",
//            "Date",
//            "Expect",
//            "Host",
//            "If-Modified-Since",
//            "Keep-Alive",
//            "Proxy-Connection",
//            "Range",
//            "Referer",
//            "Transfer-Encoding",
//            "User-Agent"
//        };

//        private static readonly Dictionary<string, PropertyInfo> HeaderProperties =
//            new Dictionary<string, PropertyInfo>();

//        static HttpWebRequestEx()
//        {
//            var type = typeof(HttpWebRequest);
//            foreach (var header in BlacklistedHeaders)
//                HeaderProperties[header] = type.GetProperty(header.Replace("-", string.Empty));
//        }

//        private static string ProcessEnumToString(string original)
//        {
//            var sb = new StringBuilder();
//            var IgnoreNext = true;

//            for (var i = 0; i < original.Length; i++)
//            {
//                if (IgnoreNext)
//                {
//                    sb.Append(original[i]);
//                    IgnoreNext = false;
//                    continue;
//                }

//                if (char.IsUpper(original[i]))
//                {
//                    sb.Append($"-{original[i]}");
//                    continue;
//                }

//                sb.Append(original[i]);
//            }

//            return sb.ToString();
//        }

//        public static string ToString(this HttpRequestHeader header, bool Requestify)
//        {
//            var original = header.ToString();
//            if (!Requestify) return original;

//            return ProcessEnumToString(original);
//        }

//        public static string ToString(this HttpResponseHeader header, bool Responsify)
//        {
//            var original = header.ToString();
//            if (!Responsify) return original;

//            return ProcessEnumToString(original);
//        }

//        public static void SetHeaders(this HttpWebRequest request, Dictionary<object, object> HeaderCollection)
//        {
//            if (HeaderCollection == null)
//                return;
//            if (HeaderCollection.Count == 0)
//                return;

//            foreach (var h in HeaderCollection)
//                request.SetHeader(h.Key, h.Value);
//        }

//        public static void SetHeader(this HttpWebRequest request, object key, object Value)
//        {
//            if (key == null)
//                return;

//            if (!(key is HttpRequestHeader) && !(key is HttpResponseHeader) && !(key is string))
//                return;

//            var Key = string.Empty;

//            if (key is HttpRequestHeader)
//                Key = ((HttpRequestHeader) key).ToString(true);
//            else if (key is HttpResponseHeader)
//                Key = ((HttpResponseHeader) key).ToString(true);
//            else
//                Key = key.ToString();

//            if (HeaderProperties.ContainsKey(Key))
//            {
//                var property = HeaderProperties[Key];

//                if (Value == null)
//                {
//                    request.SetHeaderNull(property);
//                }
//                else
//                {
//                    if (property.PropertyType.IsInstanceOfType(Value))
//                    {
//                        property.SetValue(request, Value, null);
//                    }
//                    else if (Value is string)
//                    {
//                        var value = Value.ToString();
//                        if (property.PropertyType == typeof(DateTime))
//                            property.SetValue(request, DateTime.Parse(value), null);
//                        else if (property.PropertyType == typeof(bool))
//                            property.SetValue(request, bool.Parse(value), null);
//                        else if (property.PropertyType == typeof(long))
//                            property.SetValue(request, long.Parse(value), null);
//                        else
//                            property.SetValue(request, value, null);
//                    }
//                }
//            }
//            else
//            {
//                request.Headers.Set(Key, Value.ToString());
//            }
//        }

//        private static void SetHeaderNull(this HttpWebRequest request, PropertyInfo property)
//        {
//            if (property == null)
//                return;

//            var type = property.PropertyType;

//            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
//            {
//                property.SetValue(request, null, null);
//            }
//            else
//            {
//                var i = Activator.CreateInstance(type);
//                property.SetValue(request, i, null);
//            }
//        }
//    }
//}


