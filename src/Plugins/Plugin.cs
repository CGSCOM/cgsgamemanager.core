﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Extensions;

namespace CGSGameManager.Core.Plugins
{
    public abstract class Plugin
    {
        private readonly Hash<string, HashSet<DynamicMethodInfo>> _pluginsHooks;

        protected Plugin()
        {
            _pluginsHooks = new Hash<string, HashSet<DynamicMethodInfo>>();
        }

        public bool IsLoaded { get; internal set; }
        public string Name { get; protected set; }
        public VersionNumber Version { get; protected set; }
        public AssemblyName ReferencingAssembly => Assembly.GetAssembly(GetType()).GetName();
        public Extension Extension { get; internal set; }

        internal void Load()
        {
            GenerateHooks();
            IsLoaded = true;
            lock (_pluginsHooks)
            {
                LogInfo($"Registered {_pluginsHooks.Count} Hooks");
            }

            Call("Init", null);
        }

        [HookMethod("OnRemoved", MaxExecutionTime = 20)]
        internal void OnRemoved()
        {
            Call("Unload", null);
            lock (_pluginsHooks)
            {
                _pluginsHooks.Clear();
            }
        }

        protected bool QueueWorkerThread(Action callback)
        {
            return Core.QueueAsyncAction(callback);
        }

        protected bool QueueWorkerThread(WaitCallback callback, object state)
        {
            return Core.QueueAsyncAction(callback, state);
        }

        protected void NextTick(Action action)
        {
            Core.NextFrame(action);
        }

        public object Call(string hookname, object[] args)
        {
            lock (_pluginsHooks)
            {
                if (!_pluginsHooks.TryGetValue(hookname, out var methods)) return null;

                if (methods.Count == 1)
                {
                    var call = methods.First();
                    if (!call.HasSignatureMatch(args)) return null;
                    try
                    {
                        return call.Call(this, args);
                    }
                    catch (Exception ex)
                    {
                        if (ex is TargetInvocationException && ex.InnerException != null) ex = ex.InnerException;
                        LogException($"Failed to Call Hook {hookname} on Plugin {Name}", ex);
                    }

                    return null;
                }

                var callenum = methods.Where(a => a.HasSignatureMatch(args));

                if (ArrayPool.IsArraySingleValue(null, args) && callenum.Count() > 1)
                    return null;

                var calls = callenum.ToList();

                var returns = ArrayPool.Get(calls.Count);

                var n = 0;
                for (var i = 0; i < returns.Length; i++)
                    try
                    {
                        var r = calls[i].Call(this, args);
                        if (r == null) continue;
                        returns[n] = r;
                        n++;
                    }
                    catch (Exception e)
                    {
                        if (e is TargetInvocationException && e.InnerException != null) e = e.InnerException;

                        LogException($"Failed to Call Hook {hookname} on Plugin {Name}", e);
                    }

                var value = n == 0 ? null : returns[0];
                ArrayPool.Free(returns);
                return value;
            }
        }

        private void GenerateHooks()
        {
            var methods = GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var m in methods)
            {
                if (m.Name.Contains("get_") || m.Name.Contains("set_") || m.DeclaringType != GetType() ||
                    m.IsVirtual) continue;

                var a =
                    m.GetCustomAttributes(typeof(HookMethodAttribute), false).FirstOrDefault() as HookMethodAttribute;

                var hookname = a?.HookName ?? m.Name;

                lock (_pluginsHooks)
                {
                    if (!_pluginsHooks.ContainsKey(hookname))
                        _pluginsHooks[hookname] = new HashSet<DynamicMethodInfo>();

                    _pluginsHooks[hookname].Add(new DynamicMethodInfo(m, hookname, a?.MaxExecutionTime ?? 20));
                }
            }
        }

        #region References

        protected CGSManager Core => Interface.Core;

        protected bool DEBUG => CGSManager.DEBUGMODE;

        #endregion

        #region Logging

        protected void LogInfo(string message)
        {
            Interface.Core.LogInfo($"[{Name}] {message}");
        }

        protected void LogWarning(string message)
        {
            Interface.Core.LogWarning($"[{Name}] {message}");
        }

        protected void LogError(string message)
        {
            Interface.Core.LogError($"[{Name}] {message}");
        }

        protected void LogException(string message, Exception ex)
        {
            Interface.Core.LogException($"[{Name}] {message}", ex);
        }

        #endregion
    }
}
