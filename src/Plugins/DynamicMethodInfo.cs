﻿using System;
using System.Linq;
using System.Reflection;
using CGSGameManager.Core.Logging;

namespace CGSGameManager.Core.Plugins
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public sealed class HookMethodAttribute : Attribute
    {
        public string HookName;
        public long MaxExecutionTime = 20;

        public HookMethodAttribute(string HookName)
        {
            this.HookName = HookName;
        }

        public HookMethodAttribute(string hookname, long MaxDir = 20) : this(hookname)
        {
            MaxExecutionTime = MaxDir;
        }
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public sealed class HookIgnoreAttribute : Attribute
    {
    }

    public sealed class DynamicMethodInfo
    {
        private readonly long MaxExecutionDiration;
        private readonly MethodInfo Method;
        private readonly string NameOverride;
        private Type[] ParameterTypes;

        public DynamicMethodInfo(MethodInfo info, string name = null, long maxdiration = 20)
        {
            Method = info;
            NameOverride = name ?? info.Name;
            MaxExecutionDiration = maxdiration;
            var parameters = info.GetParameters();

            if (parameters.Length == 0)
            {
                ParameterCount = 0;
                ParameterTypes = new Type[0];
            }
            else
            {
                ParameterCount = parameters.Length;
                ParameterTypes = parameters.Select(p => p.ParameterType).ToArray();
            }
        }

        public int ParameterCount { get; }

        public object Call(object instance, object[] args)
        {
            using (TimedWarning.New(NameOverride, MaxExecutionDiration))
            {
                return Method.Invoke(instance, args);
            }
        }

        public bool HasSignatureMatch(object[] args)
        {
            switch (args)
            {
                case null when ParameterCount > 0:
                    return false;
                case null when ParameterCount == 0:
                    return true;
            }

            var callingcount = args.Length;

            for (var i = 0; i < callingcount; i++)
            {
                var c = args[i];
                var t = Method.GetParameters().FirstOrDefault(p => p.Position == i);
                switch (c)
                {
                    case null when t == null:
                        continue;

                    case null when t.IsOptional:
                        args[i] = t.DefaultValue;
                        continue;

                    case null when IsNullable(t.ParameterType):
                        continue;
                }

                if (t.ParameterType.IsInstanceOfType(c)) continue;

                return false;
            }

            return true;
        }

        private static bool IsNullable(Type type)
        {
            return type != null && !type.IsValueType;
        }
    }
}
