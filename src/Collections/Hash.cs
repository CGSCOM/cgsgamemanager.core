﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CGSGameManager.Core.Collections
{
    [Serializable]
    public class Hash<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _dictionary;

        public Hash()
        {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        public TValue this[TKey key]
        {
            get
            {
                lock (_dictionary)
                {
                    if (!_dictionary.TryGetValue(key, out var val))
                        return typeof(TValue).IsValueType ? Activator.CreateInstance<TValue>() : default(TValue);
                    if (val.Equals(default(TValue))) _dictionary.Remove(key);
                    return val;
                }
            }

            set
            {
                lock (_dictionary)
                {
                    if (value == null) _dictionary.Remove(key);
                    else _dictionary[key] = value;
                }
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                lock (_dictionary)
                {
                    return _dictionary.Keys;
                }
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                lock (_dictionary)
                {
                    return _dictionary.Values;
                }
            }
        }

        public int Count
        {
            get
            {
                lock (_dictionary)
                {
                    return _dictionary.Count;
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                lock (_dictionary)
                {
                    return _dictionary.IsReadOnly;
                }
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            lock (_dictionary)
            {
                return _dictionary.GetEnumerator();
            }
        }

        public bool ContainsKey(TKey key)
        {
            lock (_dictionary)
            {
                return _dictionary.ContainsKey(key);
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> pair)
        {
            lock (_dictionary)
            {
                return _dictionary.Contains(pair);
            }
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] pair, int index)
        {
            lock (_dictionary)
            {
                _dictionary.CopyTo(pair, index);
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (_dictionary)
            {
                return _dictionary.TryGetValue(key, out value);
            }
        }

        public void Add(KeyValuePair<TKey, TValue> pair)
        {
            lock (_dictionary)
            {
                _dictionary.Add(pair);
            }
        }

        public void Add(TKey key, TValue value)
        {
            lock (_dictionary)
            {
                _dictionary.Add(key, value);
            }
        }

        public bool Remove(TKey key)
        {
            lock (_dictionary)
            {
                return _dictionary.Remove(key);
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> pair)
        {
            lock (_dictionary)
            {
                return _dictionary.Remove(pair);
            }
        }

        public void Clear()
        {
            lock (_dictionary)
            {
                _dictionary.Clear();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
