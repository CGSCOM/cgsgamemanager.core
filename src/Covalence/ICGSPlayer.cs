﻿using System;
using System.Net;

namespace CGSGameManager.Core.Covalence
{
    public enum CommandType
    {
        /// <summary>
        ///     Chat command
        /// </summary>
        Chat,

        /// <summary>
        ///     Console command
        /// </summary>
        Console
    }

    /// <summary>
    ///     Defines a position in 3D Space
    /// </summary>
    public struct Vector3Position
    {
        public const float Epsilon = 1E-05f;

        #region Constants

        public static readonly Vector3Position Zero = new Vector3Position(0, 0, 0);

        public static readonly Vector3Position Forward = new Vector3Position(0, 0, 1);

        public static readonly Vector3Position Backward = new Vector3Position(0, 0, -1);

        public static readonly Vector3Position Up = new Vector3Position(0, 1, 0);

        public static readonly Vector3Position Down = new Vector3Position(0, -1, 0);

        public static readonly Vector3Position Left = new Vector3Position(-1, 0, 0);

        public static readonly Vector3Position Right = new Vector3Position(1, 0, 0);

        #endregion

        #region Operaters

        public static Vector3Position operator +(Vector3Position a, Vector3Position b)
        {
            return new Vector3Position(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3Position operator -(Vector3Position a, Vector3Position b)
        {
            return new Vector3Position(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static Vector3Position operator -(Vector3Position a)
        {
            return new Vector3Position(-a.X, -a.Y, -a.Z);
        }

        #endregion

        public readonly float X, Y, Z;

        public Vector3Position(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}, Z: {Z}";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3Position position))
                return false;

            return X.Equals(position.X) && Y.Equals(position.Y) && Z.Equals(position.Z);
        }

        public static float Distance(Vector3Position a, Vector3Position b)
        {
            var c = new Vector3Position(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            return (float) Math.Sqrt(c.X * c.X + c.Y * c.Y + c.Z * c.Z);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }

    /// <summary>
    ///     The Base Interface that defines a single player
    /// </summary>
    public interface ICgsPlayer : IEquatable<ICgsPlayer>
    {
        #region Information

        /// <summary>
        ///     How the user last executed a command
        /// </summary>
        CommandType LastCommand { get; }

        /// <summary>
        ///     Readonly SteamID of the player
        /// </summary>
        ulong SteamId { get; }

        /// <summary>
        ///     Associated Luma/OwnerID of the player
        /// </summary>
        ulong OwnerId { get; }

        /// <summary>
        ///     Username of the player
        /// </summary>
        string UserName { get; set; }

        /// <summary>
        ///     Players selected language
        /// </summary>
        string Language { get; }

        /// <summary>
        ///     Players connection address
        /// </summary>
        IPEndPoint Address { get; }

        /// <summary>
        ///     Steam authentication status
        /// </summary>
        bool Authenticated { get; }

        /// <summary>
        ///     Is this player currently connected
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        ///     Is this player a admin in game
        /// </summary>
        bool IsAdmin { get; }

        /// <summary>
        ///     Is this player banned
        /// </summary>
        bool IsBanned { get; }

        /// <summary>
        ///     The players position on the map
        /// </summary>
        Vector3Position Position { get; set; }

        /// <summary>
        ///     The players current session duration
        /// </summary>
        uint SessionTime { get; }

        #endregion

        #region Administration

        /// <summary>
        ///     Kicks a player from the server
        /// </summary>
        /// <param name="reason">Optional Reason</param>
        void Kick(string reason = null);

        /// <summary>
        ///     Ban a player for a specific amount of time
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="span"></param>
        void Ban(string reason, TimeSpan span);

        /// <summary>
        ///     Ban a player permanently
        /// </summary>
        /// <param name="reason"></param>
        void Ban(string reason);

        /// <summary>
        ///     Unbans this user if banned
        /// </summary>
        void Unban();

        /// <summary>
        ///     Teleports this player to a specific location
        /// </summary>
        /// <param name="position"></param>
        void Teleport(Vector3Position position);

        /// <summary>
        ///     Teleports this player to another player
        /// </summary>
        /// <param name="player"></param>
        void Teleport(ICgsPlayer player);

        /// <summary>
        ///     Teleports this player to a specific location
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        void Teleport(float x, float y, float z);

        #endregion

        #region Chat

        /// <summary>
        ///     Send this player a chat message
        /// </summary>
        /// <param name="message"></param>
        void SendChatMessage(string message);

        /// <summary>
        ///     Send this player a formatted message
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        void SendChatMessage(string format, params object[] args);

        /// <summary>
        ///     Send a message as this player
        /// </summary>
        /// <param name="message"></param>
        void SendMessageAsPlayer(string message);

        /// <summary>
        ///     Send a formatted message as this player
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        void SendMessageAsPlayer(string format, params object[] args);

        /// <summary>
        ///     Replies to the player via the last message type
        /// </summary>
        /// <param name="message"></param>
        void Reply(string message);

        /// <summary>
        ///     Replies to a player with a formatted message via last method a command was executed
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        void Reply(string format, params object[] args);

        #endregion
    }
}
