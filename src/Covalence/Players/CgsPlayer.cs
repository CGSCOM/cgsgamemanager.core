﻿using System;
using System.Linq;
using System.Net;
using System.Text;

namespace CGSGameManager.Core.Covalence.Players
{
    public abstract class CgsPlayer : ICgsPlayer
    {

        protected CgsPlayer()
        {
        }

        #region Information

        /// <inheritdoc />
        public virtual ulong SteamId { get; protected set; }

        /// <inheritdoc />
        public virtual ulong OwnerId { get; protected set; }

        /// <inheritdoc />
        public abstract string UserName { get; set; }

        /// <inheritdoc />
        public abstract string Language { get; set; }

        /// <inheritdoc />
        public abstract IPEndPoint Address { get; }

        /// <inheritdoc />
        public Vector3Position Position
        {
            get => GetCurrentPosition();
            set => Teleport(value);
        }

        /// <inheritdoc />
        public abstract bool IsAdmin { get; }

        /// <inheritdoc />
        public bool IsBanned => Interface.Core.CovalenceProvider.GetPlayerManager().IsBanned(this);

        /// <inheritdoc />
        public abstract bool IsConnected { get; }

        /// <inheritdoc />
        public virtual bool Authenticated { get; protected set; }

        /// <inheritdoc />
        public abstract CommandType LastCommand { get; }

        public abstract uint SessionTime { get; }

        #endregion

        #region Administration

        /// <inheritdoc />
        public void Ban(string reason)
        {
            Ban(reason, default(TimeSpan));
        }

        /// <inheritdoc />
        public void Ban(string reason, TimeSpan duration)
        {
            Interface.Core.CovalenceProvider?.GetPlayerManager()?.Ban(this, reason, duration);
        }

        public void Unban()
        {
            Interface.Core.CovalenceProvider.GetPlayerManager().Unban(this);
        }

        /// <inheritdoc />
        public void Kick(string reason)
        {
            Interface.Core.CovalenceProvider.GetPlayerManager().Kick(this, reason);
        }

        /// <inheritdoc />
        public abstract void Teleport(float x, float y, float z);

        /// <inheritdoc />
        public void Teleport(Vector3Position postion)
        {
            Teleport(postion.X, postion.Y, postion.Z);
        }

        /// <inheritdoc />
        public void Teleport(ICgsPlayer player)
        {
            if (player == null)
                return;

            Teleport(player.Position);
        }

        /// <inheritdoc />
        public abstract void SendChatMessage(string message);

        /// <inheritdoc />
        public void SendChatMessage(string message, params object[] args)
        {
            if (args == null || !args.Any()) SendChatMessage(message);
            else SendChatMessage(string.Format(message, args));
        }

        /// <inheritdoc />
        public abstract void SendMessageAsPlayer(string message);

        /// <inheritdoc />
        public void SendMessageAsPlayer(string message, params object[] args)
        {
            if (args == null || !args.Any()) SendMessageAsPlayer(message);
            else SendMessageAsPlayer(string.Format(message, args));
        }

        /// <inheritdoc />
        public abstract void Reply(string message);

        /// <inheritdoc />
        public void Reply(string message, params object[] args)
        {
            if (args == null || !args.Any()) Reply(message);
            else Reply(string.Format(message, args));
        }

        /// <summary>
        /// A override helper to get the current position of the player
        /// </summary>
        /// <returns>Current Position in 3D Space</returns>
        protected abstract Vector3Position GetCurrentPosition();

        #endregion

        #region Object Overloads

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder()
                .AppendFormat("{0}[SID: {1}, NAME: {2}", GetType().Name, SteamId, UserName);

            if (Address != null)
            {
                sb.AppendFormat(", IP: {0}", Address);
            }

            if (IsAdmin)
                sb.Append(", IsAdmin");

            if (IsConnected)
            {
                if (!Authenticated)
                    sb.Append(", AUTH-FAILED");

                sb.Append(", Connected");

                if (SessionTime > 0)
                {
                    var timespan = TimeSpan.FromSeconds(SessionTime);
                    sb.Append("(");
                    if (timespan.Days > 0)
                        sb.AppendFormat("{0}d, ", timespan.Days);

                    if (timespan.Hours > 0)
                        sb.AppendFormat("{0}h, ", timespan.Hours);

                    if (timespan.Minutes > 0)
                        sb.AppendFormat("{0}m, ", timespan.Minutes);

                    sb.AppendFormat("{0}s)", timespan.Seconds);
                }
            }

            sb.Append("]");

            return sb.ToString().Trim();
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return obj is ICgsPlayer player && player.SteamId == SteamId;
        }

        /// <inheritdoc />
        public virtual bool Equals(ICgsPlayer player) => player?.SteamId == SteamId;

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return SteamId.GetHashCode();
        }

        #endregion
    }
}
