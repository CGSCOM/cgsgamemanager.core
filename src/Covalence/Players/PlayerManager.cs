﻿using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Covalence.Generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace CGSGameManager.Core.Covalence.Players
{
    public abstract class PlayerManager<TPlayer, TGamePlayer> : IPlayerManager<TPlayer>
        where TPlayer : ICgsPlayer
        where TGamePlayer : class
    {
        private const string DataFileName = "UserData.cache";
        private readonly string _dataFile;

        protected PlayerManager(Func<TGamePlayer, TPlayer> converter)
        {
            ConvertFromGamePlayer = converter ?? throw new ArgumentNullException(nameof(converter),
                                        "A converter needs to be set to convert the game player to a universal player");
            Connected = new Hash<ulong, TPlayer>();
            All = new Hash<ulong, TPlayer>();
            UserData = new Hash<ulong, UserData>();
            _dataFile = Path.Combine(Interface.Core.DataDirectory.FullName, DataFileName);
        }

        protected Func<TGamePlayer, TPlayer> ConvertFromGamePlayer { get; }

        #region Maintainence

        /// <inheritdoc />
        public virtual void Load()
        {
            if (!File.Exists(_dataFile)) return;

            try
            {
                var formatter = new BinaryFormatter();
                using (var fs = File.OpenRead(_dataFile))
                using (var cs = new DeflateStream(fs, CompressionMode.Decompress))
                {
                    var data = (Hash<ulong, UserData>) formatter.Deserialize(cs);

                    if (data.Count == 0) return;


                    UserData.Clear();

                    foreach (var ud in data) UserData.Add(ud);
                }

                Interface.Core.LogInfo($"[PlayerManager] Datafile loaded! {UserData.Count} total users");
            }
            catch (Exception ex)
            {
                Interface.Core.LogException("[PlayerManager] Datafile failed to load", ex);
            }
        }

        /// <inheritdoc />
        public virtual void Save()
        {
            if (UserData == null || UserData?.Count == 0) return;

            try
            {
                var formatter = new BinaryFormatter();
                using (var fs = File.OpenWrite(_dataFile))
                using (var cs = new DeflateStream(fs, CompressionMode.Compress))
                {
                    formatter.Serialize(cs, UserData);
                }

                Interface.Core.LogInfo($"[PlayerManager] Datafile saved! {UserData.Count} total users");
            }
            catch (Exception ex)
            {
                Interface.Core.LogException("[PlayerManager] Datafile failed to save", ex);
            }
        }

        /// <inheritdoc />
        public virtual void Dispose()
        {
            Save();
            UserData.Clear();
            Connected.Clear();
            All.Clear();
        }

        #endregion

        #region Properties

        /// <inheritdoc />
        public Hash<ulong, UserData> UserData { get; }

        /// <inheritdoc />
        public Hash<ulong, TPlayer> Connected { get; }

        /// <inheritdoc />
        public Hash<ulong, TPlayer> All { get; }

        #endregion

        #region Player Management

        public void OnPlayerConnected(TGamePlayer gameplayer)
        {
            if (gameplayer == null)
                return;

            var player = ConvertFromGamePlayer(gameplayer);

            var cgsPlayer = player as CgsPlayer;
        }

        /// <inheritdoc />
        public abstract void Kick(TPlayer player, string reason);

        /// <inheritdoc />
        public void Kick(TPlayer player) => Kick(player, "No Reason");

        /// <inheritdoc />
        public void Kick(ICgsPlayer player, string reason)
        {
            if (player is TPlayer converted)
                Kick(converted, reason);
        }

        /// <inheritdoc />
        public void Kick(ICgsPlayer player) => Kick(player, "No Reason");

        /// <inheritdoc />
        public abstract void Ban(TPlayer player, string reason, TimeSpan duration);

        /// <inheritdoc />
        public void Ban(TPlayer player, string reason) => Ban(player, reason, default(TimeSpan));

        /// <inheritdoc />
        public void Ban(TPlayer player) => Ban(player, "No Reason");

        /// <inheritdoc />
        public void Ban(ICgsPlayer player, string reason, TimeSpan duration)
        {
            if (player is TPlayer converted)
                Ban(converted, reason, duration);
        }

        /// <inheritdoc />
        public void Ban(ICgsPlayer player, string reason) => Ban(player, reason, default(TimeSpan));

        /// <inheritdoc />
        public void Ban(ICgsPlayer player) => Ban(player, "No Reason");

        /// <inheritdoc />
        public abstract bool IsBanned(TPlayer player);

        /// <inheritdoc />
        public bool IsBanned(ICgsPlayer player)
        {
            if (player is TPlayer converted)
                return IsBanned(converted);
            return false;
        }

        /// <inheritdoc />
        public abstract void Unban(TPlayer player);

        /// <inheritdoc />
        public void Unban(ICgsPlayer player)
        {
            if (player is TPlayer converted)
                Unban(converted);
        }

        /// <inheritdoc />
        public abstract void SendMessageTo(TPlayer player, string message);

        /// <inheritdoc />
        public void SendMessageTo(TPlayer player, string format, params object[] args)
        {
            SendMessageTo(player, string.Format(format, args));
        }

        #endregion

        #region Player Finding

        /// <inheritdoc />
        public ICgsPlayer Find(ulong steamId)
        {
            return FindPlayer(steamId);
        }

        /// <inheritdoc />
        public ICgsPlayer Find(string name)
        {
            return FindPlayer(name);
        }

        /// <inheritdoc />
        public IEnumerable<ICgsPlayer> FindMultiple(string name, bool all = false)
        {
            return FindPlayers(name, all).Cast<ICgsPlayer>();
        }

        /// <inheritdoc />
        public TPlayer FindPlayer(ulong steamId)
        {
            return Connected == null ? default(TPlayer) :
                Connected.TryGetValue(steamId, out var player) ? player : default(TPlayer);
        }

        /// <inheritdoc />
        public TPlayer FindPlayer(string name)
        {
            return FindPlayers(name).FirstOrDefault();
        }

        /// <inheritdoc />
        public IEnumerable<TPlayer> FindPlayers(string name, bool all = false)
        {
            if (All == null) yield break;

            foreach (var player in All.Values)
            {
                if (!all && !player.IsConnected) continue;

                if (string.IsNullOrEmpty(name))
                {
                    yield return player;
                    continue;
                }

                if (player.UserName.ToLower().Contains(name.ToLower())) yield return player;
            }
        }

        #endregion
    }
}
