﻿namespace CGSGameManager.Core.Covalence
{
    public delegate void CommandCallback(ICommandContext context);

    public interface ICommandProvider
    {
        /// <summary>
        /// Check if a command exists
        /// </summary>
        /// <param name="command">Short or full command name</param>
        /// <returns>True if exists</returns>
        bool CommandExist(string command);

        /// <summary>
        /// Check if a command exists
        /// </summary>
        /// <param name="command">Short name</param>
        /// <param name="category">Category for the command</param>
        /// <returns>True if exists</returns>
        bool CommandExist(string category, string command);

        /// <summary>
        /// Check if a command exists
        /// </summary>
        /// <param name="command">Command instance</param>
        /// <returns>True if exists</returns>
        bool CommandExist(ICommand command);

        /// <summary>
        /// Registers a command with the command provider
        /// </summary>
        /// <param name="command">Short name</param>
        /// <param name="category">Category for the command</param>
        /// <param name="callback">Command callback for this command</param>
        /// <param name="description">Short help text for this command</param>
        /// <returns>True for success</returns>
        bool RegisterCommand(string category, string command, CommandCallback callback, string description);

        /// <summary>
        /// Registers multiple commands aliases with the command provider
        /// </summary>
        /// <param name="commands">Array of short name aliases</param>
        /// <param name="category">Category for the command</param>
        /// <param name="callback">Command callback for this command</param>
        /// <param name="description">Short help text for this command</param>
        /// <returns>True for success</returns>
        bool RegisterCommand(string category, string[] commands, CommandCallback callback, string description);

        /// <summary>
        /// Unregisters a command with the command provider
        /// </summary>
        /// <param name="category">Category for the command</param>
        /// <param name="command">Short name for the command</param>
        /// <returns>True on success</returns>
        bool UnregisterCommand(string category, string command);
    }
}
