﻿using System;

namespace CGSGameManager.Core.Covalence
{
    [Serializable]
    public sealed class UserData
    {
        public string DisplayName;
        public long FirstSeen;
        public string HardwareId;
        public string IpAddress;
        public long LastSeen;
        public ulong OwnerId;
        public long SecondsConnected;
        public ulong SteamId;
        public bool WasAuthenticatedLast;

        public void OnConnected(ICgsPlayer player)
        {
            var jointime = Utility.GetUnixTime();
            if (FirstSeen == default(long)) FirstSeen = jointime;
            LastSeen = jointime;
            DisplayName = player.UserName;
            IpAddress = player.Address.Address.ToString();
            WasAuthenticatedLast = player.Authenticated;
            OwnerId = player.OwnerId;
            SteamId = player.SteamId;
        }

        public void OnDisconnected(ICgsPlayer player)
        {
            var disconnectTime = Utility.GetUnixTime();
            var sessionTime = disconnectTime - LastSeen;
            SecondsConnected += sessionTime;
            DisplayName = player.UserName;
        }
    }
}
