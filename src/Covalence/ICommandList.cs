﻿namespace CGSGameManager.Core.Covalence
{
    public interface ICommandList
    {
        /// <summary>
        /// Registers messages with the translation matrix
        /// </summary>
        void RegisterMessages();

        /// <summary>
        /// Retrieves a message from the translation matrix
        /// </summary>
        /// <param name="key">Message key to get</param>
        /// <param name="player">Player used for language code</param>
        /// <param name="args">replacement arguments for message</param>
        /// <returns></returns>
        string GetMessage(string key, ICgsPlayer player, params object[] args);

        /// <summary>
        /// Retrieves a message from the translation matrix
        /// </summary>
        /// <param name="key">Message key to get</param>
        /// <param name="context">Context used for language code</param>
        /// <param name="args">replacement arguments for message</param>
        /// <returns></returns>
        string GetMessage(string key, ICommandContext context, params object[] args);
    }
}
