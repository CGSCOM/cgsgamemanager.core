﻿using System;

namespace CGSGameManager.Core.Covalence
{
    public interface ICovalenceProvider : IDisposable
    {
        /// <summary>
        ///     The Name of the current game
        /// </summary>
        string GameName { get; }

        /// <summary>
        ///     The servers steam app identifier
        /// </summary>
        uint ServerAppId { get; }

        /// <summary>
        ///     The clients steam app identifer
        /// </summary>
        uint ClientAppId { get; }

        /// <summary>
        ///     Creates the CovalenceProvider
        /// </summary>
        void Create();

        /// <summary>
        ///     Returns the current CommandProvider
        /// </summary>
        /// <returns></returns>
        ICommandProvider GetCommandProvider();

        /// <summary>
        ///     Returns the current PlayerManager
        /// </summary>
        /// <returns></returns>
        IPlayerManager GetPlayerManager();

        /// <summary>
        ///     Returns the current ServerManager
        /// </summary>
        /// <returns></returns>
        IServerManager GetServer();
    }
}
