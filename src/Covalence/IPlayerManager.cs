﻿using System;
using System.Collections.Generic;
using CGSGameManager.Core.Collections;

namespace CGSGameManager.Core.Covalence
{
    public interface IPlayerManager : IDisposable
    {
        #region Data Management

        /// <summary>
        ///     Currently loaded user data files
        /// </summary>
        Hash<ulong, UserData> UserData { get; }

        /// <summary>
        ///     Saves the player datafiles
        /// </summary>
        void Save();

        /// <summary>
        ///     Loads the player datafiles
        /// </summary>
        void Load();

        #endregion

        #region Player Managment

        /// <summary>
        ///     Finds a player by SteamId both online and offline
        /// </summary>
        /// <param name="steamId"></param>
        /// <returns></returns>
        ICgsPlayer Find(ulong steamId);

        /// <summary>
        ///     Finds a player by name searches online players only
        /// </summary>
        /// <param name="name">partial name of player</param>
        /// <returns>Null if multiple matches are found <see cref="FindMultiple" /></returns>
        ICgsPlayer Find(string name);

        /// <summary>
        ///     Finds players by partial name. Returns multiple matches if found
        /// </summary>
        /// <param name="name"></param>
        /// <param name="all">Include offline players</param>
        /// <returns></returns>
        IEnumerable<ICgsPlayer> FindMultiple(string name, bool all = false);

        /// <summary>
        ///     Ban a player
        /// </summary>
        /// <param name="player">Player being banned</param>
        void Ban(ICgsPlayer player);

        /// <summary>
        ///     Ban a player for a specific reason
        /// </summary>
        /// <param name="player">Player being banned</param>
        /// <param name="reason">Reason for the ban</param>
        void Ban(ICgsPlayer player, string reason);

        /// <summary>
        ///     Ban a player for a specific reason with optional timespan
        /// </summary>
        /// <param name="player">Player being banned</param>
        /// <param name="reason">Reason for the ban</param>
        /// <param name="duration">Timespan duration of the ban</param>
        void Ban(ICgsPlayer player, string reason, TimeSpan duration);

        /// <summary>
        ///     Returns if the player is banned
        /// </summary>
        /// <param name="player">Player cgs user object</param>
        /// <returns>True means the user is banned</returns>
        bool IsBanned(ICgsPlayer player);

        /// <summary>
        /// Unbans a player
        /// </summary>
        /// <param name="player">Cgs object of the user</param>
        void Unban(ICgsPlayer player);

        /// <summary>
        ///     Kicks a player from the game
        /// </summary>
        /// <param name="player">Player being kicked</param>
        void Kick(ICgsPlayer player);

        /// <summary>
        ///     Kicks a player from the game with a optional reason
        /// </summary>
        /// <param name="player">Player being kicked</param>
        /// <param name="reason">Reason to display to the user</param>
        void Kick(ICgsPlayer player, string reason);

        #endregion
    }
}
