﻿using System;
using System.Collections.Generic;

namespace CGSGameManager.Core.Covalence
{
    /// <summary>
    /// Defines a single command
    /// </summary>
    public interface ICommand : IEquatable<ICommand>, IDisposable
    {
        /// <summary>
        /// Command category and shortname
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Command Shortname
        /// </summary>
        string ShortName { get; }

        /// <summary>
        /// Command category
        /// </summary>
        string Category { get; }

        /// <summary>
        /// Command Help Text
        /// </summary>
        string Description { get; }

        /// <summary>
        /// List of command aliases
        /// </summary>
        IEnumerable<string> Aliases { get; }

        /// <summary>
        /// Calls the callback
        /// </summary>
        /// <param name="context"></param>
        void Call(ICommandContext context);
    }
}
