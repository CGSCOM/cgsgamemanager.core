﻿using System.Text;
using System.Collections.Generic;
using CGSGameManager.Core.Libraries;

namespace CGSGameManager.Core.Covalence.Commands
{
    public class DefaultCommandList : ICommandList
    {
        public static DefaultCommandList Instance { get; private set; }

        public static void Register()
        {
            if (Instance != null)
                return;

            Instance = new DefaultCommandList();
            Instance.RegisterMessages();
        }

        private readonly Translations _translation;

        private DefaultCommandList()
        {
            _translation = Interface.Core.ExtensionManager.GetLibrary<Translations>();
            var commands = Interface.Core.CovalenceProvider.GetCommandProvider();
            commands.RegisterCommand("cgs", new[] {"teleport", "tp"}, Teleport,
                "Basic teleportation command for admins");
            commands.RegisterCommand("cgs", "debug", Debug, "Enables or disabled the framework debug mode");
            commands.RegisterCommand("cgs", new[] {"version", "v"}, Version, "Shows the current framework version");
        }

        public void RegisterMessages()
        {
            _translation.RegisterMessage(new Dictionary<string, string>
            {
                ["NoPermission"] = "<color=red>You do not have access to command:</color> <color=teal>{0}</color>",
                ["UserNotFound"] = "<color=yellow>User not found:</color> <color=grey>{0}</color>",
                ["Command_TeleportUsage"] = "<color=yellow>Teleport Usage:</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName]</color> - <color=grey>Teleports you to a player</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [SteamId/PlayerName]</color> - <color=grey>Teleports a player to another player</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Teleports you to a specific location</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Teleports a player to a specific location</color>",
                ["Command_TeleportSelf"] = "<color=teal>A <color=red>admin</color> has teleported you to <color=yellow>{0}</color></color>",
                ["Command_TeleportOther"] = "<color=teal>You have teleported <color=yellow>{0}</color> to <color=yellow>{1}</color></color>",
                ["Command_TeleportReceiver"] = "<color=teal><color=yellow>{0}</color> has been teleported to you by a <color=red>admin</color></color>",
                ["Command_TeleportUnknownCoord"] = "<color=yellow>Unable to parse <color=teal>Coordinate-{0}</color> from <color=red>{1}</color></color>",
                ["Command_DebugUsage"] = "<color=yellow>Debug Usage:</color>\n" +
                                         "<color=cyan>cgs.debug</color> - <color=grey>Shows the current value for debug mode</color>\n" +
                                         "<color=cyan>cgs.debug</color> <color=orange>[on/off|1/0|true/false|enable/disable]</color> - <color=grey>Sets the debug mode value</color>",
                ["Command_DebugValue"] = "<color=teal>Debug Mode is currently {0}</color>",
                ["Command_DebugSet"] = "<color=teal>Debug Mode set to {0}</color>",
                ["Command_Version"] = "<color=#00abeb>CGSGameManager</color> <color=teal>version {0}</color>",
                ["Command_VersionExtra"] = "<color=grey><color=orange>{0}</color> v{1} - Network Protocol <color=grey>{2}</color></color>"
            });

            _translation.RegisterMessage(new Dictionary<string, string>
            {
                ["NoPermission"] = "<color=red>Du hast keinen Zugriff auf diesen Befehl:</color> <color=teal>{0}</color>",
                ["UserNotFound"] = "<color=yellow>Benutzer konnte nicht gefunden werden:</color> <color=grey>{0}</color>",
                ["Command_TeleportUsage"] = "<color=yellow>Anleitung zum Teleportieren:</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName]</color> - <color=grey>Teleportiert Dich zu einem Spieler</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [SteamId/PlayerName]</color> - <color=grey>Teleportiert einen Spieler zu einem anderen Spieler</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Teleportiert Dich an einen bestimmten Ort</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Teleportiert einen Spieler an einen bestimmten Ort</color>",
                ["Command_TeleportSelf"] = "<color=teal>A <color=red>admin</color> hat dich zu <color=yellow>{0}</color> teleportiert</color>",
                ["Command_TeleportOther"] = "<color=teal>Du hast dich teleportiert <color=yellow>{0}</color> zu <color=yellow>{1}</color></color>",
                ["Command_TeleportReceiver"] = "<color=teal>Ein <color=red>Admin</color> hat <color=yellow>{0}</color> zu dir teleportiert</color>",
                ["Command_TeleportUnknownCoord"] = "<color=yellow>Die <color=teal>Koordinaten-{0}</color> von <color=red>{1}</color> können nicht gefunden werden</color>",
                ["Command_DebugUsage"] = "<color=yellow>Anleitung für den Debug-Modus:</color>\n" +
                                         "<color=cyan>cgs.debug</color> - <color=grey>Zeigt den aktuellen Wert für den Debug-Modus an</color>\n" +
                                         "<color=cyan>cgs.debug</color> <color=orange>[on/off|1/0|true/false|enable/disable]</color> - <color=grey>Setzt einen Debug-Modus-Wert</color>",
                ["Command_DebugValue"] = "<color=teal>Debug-Modus ist derzeit {0}</color>",
                ["Command_DebugSet"] = "<color=teal>Debug-Modus auf {0} eingestellt</color>",
                ["Command_Version"] = "<color=#00abeb>CGSGameManager</color> <color=teal>Version {0}</color>",
                ["Command_VersionExtra"] = "<color=grey><color=orange>{0}</color> v{1} - Netzwerkprotokoll <color=grey>{2}</color></color>"
            }, "de");

            _translation.RegisterMessage(new Dictionary<string, string>
            {
                ["NoPermission"] = "<color=red>Vous n'avez pas accés a cette commande:</color> <color=teal>{0}</color>",
                ["UserNotFound"] = "<color=yellow>Utilisateur non trouvé:</color> <color=grey>{0}</color>",
                ["Command_TeleportUsage"] = "<color=yellow>Usage de téléportation:</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName]</color> - <color=grey>Te téléporte a un joueur</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [SteamId/PlayerName]</color> - <color=grey>Teleporte un joueur a un autre joueur</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Téléporte un joueur a un spécifique endroit</color>\n" +
                                     "<color=cyan>cgs.teleport</color> <color=orange>[SteamId/PlayerName] [Coord-X] [Coord-Y] [Coord-Z]</color> - <color=grey>Teleporte a joueur a un spécifique emplacement</color>",
                ["Command_TeleportSelf"] = "<color=teal>A <color=red>admin</color> vous a téléporté a <color=yellow>{0}</color></color>",
                ["Command_TeleportOther"] = "<color=teal>Vous avez été téléporté <color=yellow>{0}</color> to <color=yellow>{1}</color></color>",
                ["Command_TeleportReceiver"] = "<color=teal><color=yellow>{0}</color> a été téléporter vers toi par un <color=red>admin</color></color>",
                ["Command_TeleportUnknownCoord"] = "<color=yellow>Impossible d'analyser <color=teal>Coordinate-{0}</color> from <color=red>{1}</color></color>",
                ["Command_DebugUsage"] = "<color=yellow>Usage de débogage:</color>\n" +
                                         "<color=cyan>cgs.debug</color> - <color=grey>Affiche la valeur actuelle pour le mode de débogage</color>\n" +
                                         "<color=cyan>cgs.debug</color> <color=orange>[on/off|1/0|true/false|enable/disable]</color> - <color=grey>Définit la valeur du mode de débogage</color>",
                ["Command_DebugValue"] = "<color=teal>Le mode de débogage est actuellement {0}</color>",
                ["Command_DebugSet"] = "<color=teal>Le mode débogage est défini {0}</color>",
                ["Command_Version"] = "<color=#00abeb>CGSGameManager</color> <color=teal>version {0}</color>",
                ["Command_VersionExtra"] = "<color=grey><color=orange>{0}</color> v{1} - Protocole réseau <color=grey>{2}</color></color>"
            }, "fr");
        }

        private void Teleport(ICommandContext context)
        {
            if (!context.Player.IsAdmin)
            {
                context.Reply(GetMessage("NoPermission", context, context.Command));
                return;
            }

            switch (context.Arguments.Length)
            {
                case 0:
                    context.Reply(GetMessage("Command_TeleportUsage", context));
                    return;

                case 1:
                    if (context.IsServer)
                    {
                        context.Reply(GetMessage("NoPermission", context, context.Command));
                        return;
                    }

                    var target = ulong.TryParse(context.Arguments[0], out var steamid)
                        ? Interface.Core.CovalenceProvider.GetPlayerManager().Find(steamid)
                        : Interface.Core.CovalenceProvider.GetPlayerManager().Find(context.Arguments[0]);

                    if (target == null)
                    {
                        context.Reply(GetMessage("UserNotFound", context, context.Arguments[0]));
                        return;
                    }

                    context.Player.Teleport(target);

                    if (target.IsConnected)
                        target.SendChatMessage(GetMessage("Command_TeleportReceiver", target, context.Player.UserName));

                    context.Reply(GetMessage("Command_TeleportSelf", context, target.UserName));
                    return;

                case 2:
                    var source = ulong.TryParse(context.Arguments[0], out steamid)
                        ? Interface.Core.CovalenceProvider.GetPlayerManager().Find(steamid)
                        : Interface.Core.CovalenceProvider.GetPlayerManager().Find(context.Arguments[0]);

                    if (source == null)
                    {
                        context.Reply(GetMessage("UserNotFound", context, context.Arguments[0]));
                        return;
                    }

                    target = ulong.TryParse(context.Arguments[1], out steamid)
                        ? Interface.Core.CovalenceProvider.GetPlayerManager().Find(steamid)
                        : Interface.Core.CovalenceProvider.GetPlayerManager().Find(context.Arguments[1]);

                    if (target == null)
                    {
                        context.Reply(GetMessage("UserNotFound", context, context.Arguments[1]));
                        return;
                    }

                    source.Teleport(target);

                    if (source.IsConnected)
                        source.SendChatMessage(GetMessage("Command_TeleportSelf", source, target.UserName));

                    if (target.IsConnected)
                        target.SendChatMessage(GetMessage("Command_TeleportReceiver", target, source.UserName));

                    context.Reply(GetMessage("Command_TeleportOther", context, source.UserName, target.UserName));
                    return;

                case 3:
                    if (context.IsServer)
                    {
                        context.Reply(GetMessage("NoPermission", context, context.Command));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[0], out var x))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "X", context.Arguments[0]));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[1], out var y))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "Y", context.Arguments[1]));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[2], out var z))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "Z", context.Arguments[2]));
                        return;
                    }

                    context.Player.Teleport(x, y, z);
                    context.Reply(GetMessage("Command_TeleportSelf", context, $"X-{x}, Y-{y}, Z-{z}"));
                    return;

                case 4:
                    source = Interface.Core.CovalenceProvider.GetPlayerManager().Find(context.Arguments[0]);

                    if (source == null)
                    {
                        context.Reply(GetMessage("UserNotFound", context, context.Arguments[0]));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[1], out x))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "X", context.Arguments[1]));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[2], out y))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "Y", context.Arguments[2]));
                        return;
                    }

                    if (!float.TryParse(context.Arguments[3], out z))
                    {
                        context.Reply(GetMessage("Command_TeleportUnknownCoord", context, "Z", context.Arguments[3]));
                        return;
                    }

                    source.Teleport(x, y, z);

                    if (source.IsConnected)
                        source.SendChatMessage(GetMessage("Command_TeleportSelf", source, $"X-{x}, Y-{y}, Z-{z}"));

                    context.Reply(GetMessage("Command_TeleportOther", context, source.UserName, $"X-{x}, Y-{y}, Z-{z}"));
                    return;

                default:
                    context.Reply(GetMessage("Command_TeleportUsage", context));
                    return;
            }
        }

        private void Debug(ICommandContext context)
        {
            if (!context.Player.IsAdmin)
            {
                context.Reply(GetMessage("NoPermission", context, context.Command));
                return;
            }

            switch (context.Arguments.Length)
            {
                case 0:
                    context.Reply(GetMessage("Command_DebugValue", context, CGSManager.DEBUGMODE ? "<color=green>true</color>" : "<color=red>false</color>"));
                    return;

                case 1:
                    switch (context.Arguments[0].ToLowerInvariant())
                    {
                        case "on":
                        case "1":
                        case "true":
                        case "enable":
                            CGSManager.DEBUGMODE = true;
                            break;

                        default:
                            CGSManager.DEBUGMODE = false;
                            break;
                    }
                    break;

                    default:
                    context.Reply(GetMessage("Command_DebugUsage", context));
                        return;
            }

            context.Reply(GetMessage("Command_DebugSet", context, CGSManager.DEBUGMODE ? "<color=green>true</color>" : "<color=red>false</color>"));

            if (!context.IsServer && CGSManager.DEBUGMODE)
                Interface.Core.LogWarning($"Debug mode has been enabled by {context.Player.UserName}/{context.Player.SteamId}");
        }

        private void Version(ICommandContext context)
        {
            var sb = new StringBuilder().Append(GetMessage("Command_Version", context, VersionNumber.CgsCurrent()));

            var gameInfo = Interface.Core.ExtensionManager.GetLibrary<GameDetails>();

            if (gameInfo != null && !gameInfo.Game.IsNullOrWhiteSpace() && !gameInfo.Protocol.IsNullOrWhiteSpace() && !gameInfo.Version.IsNullOrWhiteSpace())
            {
                var extra = GetMessage("Command_VersionExtra", context, gameInfo.Game, gameInfo.Version,
                    gameInfo.Protocol);

                sb.AppendFormat("\n{0}", extra);
            }
            context.Reply(sb.ToString());
        }

        /// <inheritdoc />
        public string GetMessage(string key, ICgsPlayer player, params object[] args)
        {
            var message = _translation.GetMessage(key, player);

            if (!message.IsNullOrWhiteSpace() && args != null)
                message = string.Format(message, args);

            return message;
        }

        /// <inheritdoc />
        public string GetMessage(string key, ICommandContext context, params object[] args) =>
            GetMessage(key, context.Player, args);
    }
}
