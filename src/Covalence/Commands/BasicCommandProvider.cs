﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Covalence.Generic;

namespace CGSGameManager.Core.Covalence.Commands
{
    public abstract class BasicCommandProvider<TCommand, TContext> : ICommandProvider<TCommand, TContext> where TCommand : ICommand where TContext : ICommandContext
    {
        protected readonly Hash<string, List<TCommand>> _commands;

        protected BasicCommandProvider()
        {
            _commands = new Hash<string, List<TCommand>>();
        }

        #region Overload Members

        /// <inheritdoc />
        public bool CommandExist(string category, string command)
        {
            if (command.IsNullOrWhiteSpace())
                return false;

            if (category.IsNullOrWhiteSpace())
                category = "cgs";

            category = category.ToLowerInvariant();
            command = command.ToLowerInvariant();

            if (!_commands.TryGetValue(category, out var commands))
                return false;

            if (commands != null)
                return commands.Any(c => c.ShortName
                                             .Equals(command,
                                                 StringComparison.InvariantCultureIgnoreCase) ||
                                         c.Aliases.Any(a => a
                                             .Equals(command,
                                                 StringComparison.InvariantCultureIgnoreCase)));


            _commands.Remove(category);
            return false;
        }

        /// <inheritdoc />
        public bool CommandExist(string command)
        {
            if (command.IsNullOrWhiteSpace())
                return false;

            var category = "cgs";

            if (command.Contains("."))
            {
                var indexoflast = command.LastIndexOf('.');

                category = command.Substring(0, indexoflast);
                command = command.Substring(indexoflast + 1, command.Length - indexoflast);
            }

            return CommandExist(category, command);
        }

        /// <inheritdoc />
        public bool CommandExist(ICommand command) =>
            command != null && CommandExist(command.Category, command.ShortName);

        /// <inheritdoc />
        public bool CommandExist(TCommand command) => command != null && CommandExist((ICommand) command);

        /// <inheritdoc />
        public bool UnregisterCommand(string category, string command)
        {
            if (category.IsNullOrWhiteSpace())
                return false;

            if (command.IsNullOrWhiteSpace())
                return false;
            category = category.ToLowerInvariant();
            command = command.ToLowerInvariant();

            if (!CommandExist(category, command))
                return false;

            var commands = _commands[category];

            var instance = commands.FirstOrDefault(c =>
                c.ShortName.Equals(command, StringComparison.InvariantCultureIgnoreCase) ||
                c.Aliases.Any(a => a.Equals(command, StringComparison.InvariantCultureIgnoreCase)));

            if (instance == null)
                return false;

            var removed = commands.Remove(instance);

            instance.Dispose();
            return removed;
        }

        /// <inheritdoc />
        public bool RegisterCommand(string category, string command, CommandCallback callback, string description)
        {
            if (command.IsNullOrWhiteSpace() || callback == null)
                return false;

            if (category.IsNullOrWhiteSpace())
                category = "cgs";

            category = category.Trim(' ', '.').ToLowerInvariant();
            command = command.Trim(' ').ToLowerInvariant();

            if (CommandExist(category, command))
                return false;

            if (!_commands.ContainsKey(category))
                _commands[category] = new List<TCommand>();

            var c = CreateCommand(category, command, callback, description);

            if (c == null)
                return false;

            _commands[category].Add(c);
            return true;
        }

        /// <inheritdoc />
        public bool RegisterCommand(string category, string[] aliases, CommandCallback callback, string description)
        {
            if (callback == null || aliases == null || aliases.Length == 0)
                return false;

            aliases = aliases.Where(a => !a.IsNullOrWhiteSpace()).Select(a => a.Trim(' ').ToLowerInvariant()).ToArray();

            if (category.IsNullOrWhiteSpace())
                category = "cgs";
            category = category.ToLowerInvariant().Trim(' ', '.');

            if (_commands.Values.Any(a => a.Any(c =>
                c.Category.Equals(category, StringComparison.InvariantCultureIgnoreCase) &&
                c.Aliases.Any(aliases.Contains))))
                return false;

            if (!_commands.ContainsKey(category))
                _commands[category] = new List<TCommand>();

            var command = CreateCommand(category, aliases, callback, description);

            if (command == null)
                return false;

            _commands[category].Add(command);
            return true;
        }

        #endregion

        private void OnProcessCommand(ICgsPlayer player, string command, string[] args)
        {
            if (command.IsNullOrWhiteSpace() || player == null)
                return;

            if (command.StartsWith("!"))
                command = command.TrimStart('!');

            var category = string.Empty;
            var shortname = string.Empty;
            if (command.Contains("."))
            {
                var lastindexof = command.LastIndexOf('.');

                category = command.Substring(0, lastindexof).ToLowerInvariant();
                shortname = command.Substring(lastindexof + 1, command.Length - lastindexof).ToLowerInvariant();
            }
            else
            {
                category = "cgs";
                shortname = command.ToLower();
            }

            if (!_commands.TryGetValue(category, out var commands))
            {
                player.Reply("Command not found '{0}'", command);
                return;
            }

            var commandinstance = commands.FirstOrDefault(c =>
                c.Aliases
                    .Any(a => a.Equals(shortname, StringComparison.InvariantCultureIgnoreCase)));

            if (commandinstance == null)
            {
                player.Reply("Command not found '{0}'", command);
                return;
            }

            var context = CreateContext(player, commandinstance, args);

            if (context == null)
            {
                player.Reply("Failed to create command context");
                return;
            }

            commandinstance.Call(context);
        }

        /// <summary>
        /// A helper to create a command context
        /// </summary>
        /// <param name="player">The player that issued the command</param>
        /// <param name="command">The command being called</param>
        /// <param name="args">Arguments provided with the command</param>
        /// <returns>CommandContext</returns>
        protected abstract TContext CreateContext(ICgsPlayer player, TCommand command, string[] args);

        private TCommand CreateCommand(string category, string command, CommandCallback callback, string description)
        {
            if (command.IsNullOrWhiteSpace()|| category.IsNullOrWhiteSpace() || callback == null)
                return default(TCommand);

            return CreateCommand(category, new[] {command}, callback, description);
        }

        /// <summary>
        /// A helper to create commands
        /// </summary>
        /// <param name="category">Category the command belongs to</param>
        /// <param name="aliases">Aliases for this command</param>
        /// <param name="callback">Callback for this command</param>
        /// <param name="description">Help text for this command</param>
        /// <returns></returns>
        protected abstract TCommand CreateCommand(string category, string[] aliases, CommandCallback callback, string description);
    }
}
