﻿using System.Net;

namespace CGSGameManager.Core.Covalence
{
    public interface IServerManager
    {
        #region Information

        /// <summary>
        ///     Get or set the hostname of the server
        /// </summary>
        string Hostname { get; set; }

        /// <summary>
        ///     Get the public facing IPAddress and port of the server
        /// </summary>
        IPEndPoint Endpoint { get; }

        /// <summary>
        ///     Get the current player count
        /// </summary>
        int CurrentPlayers { get; }

        /// <summary>
        ///     Get the current authenticated player count
        /// </summary>
        int CurrentSteamPlayers { get; }

        /// <summary>
        ///     Get the current unauthenticated player count
        /// </summary>
        int CurrentCgsPlayers { get; }

        /// <summary>
        ///     Gets the current amount of admins online
        /// </summary>
        int CurrentOnlineAdminCount { get; }

        /// <summary>
        ///     Gets the max player count
        /// </summary>
        int MaxPlayers { get; }

        /// <summary>
        ///     Gets the current uptime for the server
        /// </summary>
        float Uptime { get; }

        #endregion

        #region Administration

        /// <summary>
        ///     Saves the server
        /// </summary>
        void Save();

        /// <summary>
        ///     Shutsdown the server
        /// </summary>
        void Shutdown();

        #endregion
    }
}
