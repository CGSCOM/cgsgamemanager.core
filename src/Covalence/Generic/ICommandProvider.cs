﻿namespace CGSGameManager.Core.Covalence.Generic
{
    public interface ICommandProvider<TCommand, TContext> : ICommandProvider where TCommand : ICommand where TContext : ICommandContext 
    {
        /// <summary>
        /// Check if a command exists
        /// </summary>
        /// <param name="command">Command instance</param>
        /// <returns>True if exists</returns>
        bool CommandExist(TCommand command);
    }
}
