﻿namespace CGSGameManager.Core.Covalence.Generic
{
    public interface ICovalenceProvider<TCommandProvider, TPlayerManager, TServerManager> : ICovalenceProvider where TCommandProvider : ICommandProvider where TPlayerManager : IPlayerManager where TServerManager : IServerManager
    {
        TCommandProvider Commands { get; }

        TPlayerManager Players { get; }

        TServerManager Server { get; }
    }
}
