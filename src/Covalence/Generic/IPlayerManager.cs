﻿using CGSGameManager.Core.Collections;
using System;
using System.Collections.Generic;

namespace CGSGameManager.Core.Covalence.Generic
{
    public interface IPlayerManager<TPlayer> : IPlayerManager where TPlayer : ICgsPlayer
    {
        #region Player Management

        /// <summary>
        ///     A list of all currently connected players
        /// </summary>
        Hash<ulong, TPlayer> Connected { get; }

        Hash<ulong, TPlayer> All { get; }

        /// <summary>
        ///     Kicks a player
        /// </summary>
        /// <param name="player"></param>
        void Kick(TPlayer player);

        /// <summary>
        ///     Kicks a player with optional reason
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        void Kick(TPlayer player, string message);

        /// <summary>
        ///     Bans a player with optional reason and duration
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <param name="duration"></param>
        void Ban(TPlayer player, string message, TimeSpan duration);

        /// <summary>
        ///     Bans a player with optional reason
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        void Ban(TPlayer player, string message);

        /// <summary>
        ///     Bans a player
        /// </summary>
        /// <param name="player"></param>
        void Ban(TPlayer player);

        /// <summary>
        ///     Unbans a player
        /// </summary>
        /// <param name="player">Cgs player object</param>
        void Unban(TPlayer player);

        /// <summary>
        ///     Checks if a player is banned
        /// </summary>
        /// <param name="player">Cgs player object</param>
        /// <returns>True if player is banned</returns>
        bool IsBanned(TPlayer player);

        #endregion

        #region Player Finding

        /// <summary>
        ///     Find a player by steamID
        /// </summary>
        /// <param name="steamId"></param>
        /// <returns></returns>
        TPlayer FindPlayer(ulong steamId);

        /// <summary>
        ///     Find a player by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        TPlayer FindPlayer(string name);

        /// <summary>
        ///     Find players with matching name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="all">Include not connected players</param>
        /// <returns></returns>
        IEnumerable<TPlayer> FindPlayers(string name, bool all = false);

        #endregion
    }
}
