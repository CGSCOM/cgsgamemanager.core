﻿using System;

namespace CGSGameManager.Core.Covalence
{
    /// <inheritdoc />
    /// <summary>
    /// The context of the command used
    /// </summary>
    public interface ICommandContext : IDisposable
    {
        /// <summary>
        /// The a user of the command
        /// </summary>
        ICgsPlayer Player { get; }

        /// <summary>
        /// Returns if the issuer of the command is a player
        /// </summary>
        bool IsPlayer { get; }

        /// <summary>
        /// Returns if the issuer of the command is the server console
        /// </summary>
        bool IsServer { get; }

        /// <summary>
        /// The original command string
        /// </summary>
        string Command { get; }

        /// <summary>
        /// The command aruments used with this command
        /// </summary>
        string[] Arguments { get; }

        /// <summary>
        /// Reply back to the command issuer
        /// </summary>
        /// <param name="message">Message to reply back with</param>
        void Reply(string message);

        /// <summary>
        /// Reply back to the command issuer
        /// </summary>
        /// <param name="message">Message to reply back with</param>
        /// <param name="args">optional replacement arguments</param>
        void Reply(string message, params object[] args);
    }
}
