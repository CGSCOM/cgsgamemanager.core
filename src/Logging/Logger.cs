﻿using System;
using System.Collections.Generic;

namespace CGSGameManager.Core.Logging
{
    public enum LogType
    {
        Info,
        Warning,
        Error,
        Debug,
        Stacktrace,
        Assert
    }

    public abstract class Logger
    {
        private readonly bool ProcessImediately;
        protected Queue<LogMessage> MessageQueue;

        protected Logger(bool processImediately)
        {
            ProcessImediately = processImediately;
            if (!processImediately) MessageQueue = new Queue<LogMessage>();
        }

        protected LogMessage CreateLogMessage(LogType type, string format, object[] args)
        {
            var msg = new LogMessage
            {
                Type = type,
                ConsoleMessage = $"[CGSGameManager]{format}",
                LogfileMessage = $"[{DateTime.Now.ToShortTimeString()}][{type}]{format}"
            };
            if (args.Length <= 0) return msg;
            msg.ConsoleMessage = string.Format(msg.ConsoleMessage, args);
            msg.LogfileMessage = string.Format(msg.LogfileMessage, args);

            return msg;
        }

        public virtual void Write(LogType type, string format, params object[] args)
        {
            var msg = CreateLogMessage(type, format, args);
            Write(msg);
        }

        internal virtual void Write(LogMessage message)
        {
            if (ProcessImediately) ProcessMessage(message);
            else MessageQueue.Enqueue(message);
        }

        protected virtual void ProcessMessage(LogMessage message)
        {
        }

        public virtual void WriteException(string message, Exception ex)
        {
            Write(LogType.Error, $"{message} ({ex.GetType().Name}): {ex.Message}");
            Write(LogType.Stacktrace, ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace);
        }

        public virtual void OnRemoved()
        {
        }

        public struct LogMessage
        {
            public LogType Type;
            public string ConsoleMessage;
            public string LogfileMessage;
        }
    }
}
