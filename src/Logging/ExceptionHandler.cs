﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace CGSGameManager.Core.Logging
{
    public class ExceptionHandler
    {
        public static Exception CleanException(Exception ex)
        {
            if (ex is TargetInvocationException && ex.InnerException != null) ex = ex.InnerException;

            return ex;
        }

        public static StackTrace GetStacktrace(Exception ex)
        {
            ex = CleanException(ex);
            var trace = new StackTrace(ex, true);
            return trace;
        }

        public static string BuildExceptionTraceMessage(Exception ex)
        {
            var trace = GetStacktrace(ex);
            ex = CleanException(ex);

            var sb = new StringBuilder();

            var problemarea = default(StackFrame);

            var frames = trace.GetFrames();

            var index = 0;
            for (var i = 0; i < frames.Length; i++)
                if (frames[i].GetMethod().DeclaringType.FullName.StartsWith("CGSGameManager"))
                {
                    problemarea = frames[i];
                    index = i + 1;
                    break;
                }

            if (problemarea != null)
            {
                sb.Append(
                    $"Error has occured in {problemarea.GetMethod().DeclaringType.FullName}::{problemarea.GetMethod().Name}(");
                var parms = problemarea.GetMethod().GetParameters();
                if (parms.Length > 0)
                    for (var p = 0; p < parms.Length; p++)
                    {
                        sb.Append($"{parms[p].ParameterType.FullName}");
                        if (p + 1 != parms.Length) sb.Append(", ");
                    }

                sb.Append($"); on line {problemarea.GetFileLineNumber()} ->");
                sb.AppendLine();
            }

            for (var b = index; b < frames.Length; b++)
            {
                var t = frames[b];
                sb.Append($"{t.GetMethod().DeclaringType.FullName}::{t.GetMethod().Name}(");

                var parms = t.GetMethod().GetParameters();
                if (parms.Length > 0)
                    for (var p = 0; p < parms.Length; p++)
                    {
                        sb.Append($"{parms[p].ParameterType.FullName}");
                        if (p + 1 != parms.Length) sb.Append(", ");
                    }

                sb.Append(");");

                if (b + 1 != frames.Length)
                {
                    sb.Append(" ->");
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }
    }
}
