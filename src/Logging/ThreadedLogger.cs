﻿using System.Threading;

namespace CGSGameManager.Core.Logging
{
    public abstract class ThreadedLogger : Logger
    {
        #region asyncs

        private readonly AutoResetEvent waitevent;
        private bool exit;
        private readonly object syncroot;

        private readonly Thread workerthread;

        #endregion


        #region Constructs

        public ThreadedLogger() : base(false)
        {
            waitevent = new AutoResetEvent(false);
            exit = false;
            syncroot = new object();
            workerthread = new Thread(Worker) {IsBackground = true, Name = "CGSThreadedLogger"};
            workerthread.Start();
        }

        ~ThreadedLogger()
        {
            OnRemoved();
        }

        public override void OnRemoved()
        {
            if (exit) return;
            exit = true;
            waitevent.Set();
            workerthread.Join();
        }

        #endregion

        #region Logger Actions

        internal override void Write(LogMessage message)
        {
            lock (syncroot)
            {
                base.Write(message);
            }

            waitevent.Set();
        }

        protected abstract void BeginBatchProcess();

        protected abstract void FinishBatchProcess();

        private void Worker()
        {
            while (!exit)
            {
                waitevent.WaitOne();

                lock (syncroot)
                {
                    if (MessageQueue.Count <= 0) continue;
                    BeginBatchProcess();
                    try
                    {
                        while (MessageQueue.Count > 0)
                        {
                            var msg = MessageQueue.Dequeue();
                            ProcessMessage(msg);
                        }
                    }
                    finally
                    {
                        FinishBatchProcess();
                    }
                }
            }
        }

        #endregion
    }
}
