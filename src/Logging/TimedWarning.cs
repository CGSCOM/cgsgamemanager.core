﻿using System;
using System.Diagnostics;

namespace CGSGameManager.Core.Logging
{
    public sealed class TimedWarning : IDisposable
    {
        private long dir;
        private string methodname;
        private Stopwatch Stopwatch;

        private TimedWarning(string methodname, long expecteddiration)
        {
            Stopwatch = new Stopwatch();
            dir = expecteddiration;
            this.methodname = methodname;
            Stopwatch.Start();
        }

        public void Dispose()
        {
            Stopwatch.Stop();
            var enddir = Stopwatch.ElapsedMilliseconds;

            if (enddir > dir) Interface.Core.LogWarning($"Calling method {methodname} took {enddir}ms");

            Stopwatch = null;
            dir = default(long);
            methodname = null;
        }

        public static TimedWarning New(string methodname, long expectedDiration = 20)
        {
            return new TimedWarning(methodname, expectedDiration);
        }
    }
}
