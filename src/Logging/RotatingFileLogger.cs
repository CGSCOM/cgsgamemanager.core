﻿using System;
using System.IO;

namespace CGSGameManager.Core.Logging
{
    public sealed class RotatingFileLogger : ThreadedLogger
    {
        private StreamWriter Writer;

        public RotatingFileLogger(string directory)
        {
            Directory = directory;
        }

        public string Directory { get; set; }

        private string GetLogFileName(DateTime date)
        {
            return Path.Combine(Directory, $"cgs_{date:yyyy-MM-dd}.log");
        }

        protected override void BeginBatchProcess()
        {
            Writer = new StreamWriter(new FileStream(GetLogFileName(DateTime.Now), FileMode.Append, FileAccess.Write));
        }

        protected override void FinishBatchProcess()
        {
            Writer.Close();
            Writer.Dispose();
        }

        protected override void ProcessMessage(LogMessage message)
        {
            Writer.WriteLine(message.LogfileMessage);
        }
    }
}
