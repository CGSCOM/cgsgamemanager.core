﻿using System;
using System.Collections.Generic;

namespace CGSGameManager.Core.Logging
{
    public sealed class CompoundLogger : Logger
    {
        private readonly DebugCallback _debugCallback;

        public CompoundLogger(DebugCallback callback) : base(true)
        {
            SubLoggers = new List<Logger>();
            _debugCallback = callback;
        }

        private List<Logger> SubLoggers { get; }

        public bool RegisterLogger(Logger logger)
        {
            if (SubLoggers.Contains(logger)) return false;

            SubLoggers.Add(logger);
            return true;
        }

        public bool UnregisterLogger(Logger logger)
        {
            return SubLoggers.Remove(logger);
        }

        protected override void ProcessMessage(LogMessage message)
        {
            foreach (var logger in SubLoggers) logger.Write(message);

            if (_debugCallback == null)
                return;

            try
            {
                _debugCallback(message);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public override void OnRemoved()
        {
            foreach (var logger in SubLoggers) logger.OnRemoved();
            SubLoggers.Clear();
        }
    }
}
