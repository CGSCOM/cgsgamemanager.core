﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CGSGameManager.Core
{
    public static class ArrayPool
    {
        private const int MaxArrayLength = 50;
        private const int InitialPoolAmount = 64;
        private const int MaxPoolAmount = 256;

        private static readonly List<Queue<object[]>> _pooledArrays;

        static ArrayPool()
        {
            _pooledArrays = new List<Queue<object[]>>();

            for (var i = 0; i < MaxArrayLength; i++)
            {
                _pooledArrays.Add(new Queue<object[]>());
                SetupArrays(i + 1);
            }
        }

        /// <summary>
        ///     Gets an array of desired length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static object[] Get(int length)
        {
            if (length == 0 || length > MaxArrayLength) return new object[length];

            var arrays = _pooledArrays[length - 1];

            lock (arrays)
            {
                if (arrays.Count == 0) SetupArrays(length);

                return arrays.Dequeue();
            }
        }

        /// <summary>
        ///     Returns the array back to the array pool or destroys it if the pool is full
        /// </summary>
        /// <param name="array"></param>
        public static void Free(object[] array)
        {
            if (array == null || array.Length == 0 || array.Length > MaxArrayLength) return;

            for (var i = 0; i < array.Length; i++) array[i] = null;

            var arrays = _pooledArrays[array.Length - 1];

            lock (arrays)
            {
                if (arrays.Count > MaxPoolAmount)
                {
                    for (var i = 0; i < MaxPoolAmount; i++) arrays.Dequeue();
                    return;
                }

                arrays.Enqueue(array);
            }
        }

        private static void SetupArrays(int length)
        {
            var arrays = _pooledArrays[length - 1];

            for (var i = 0; i < InitialPoolAmount; i++) arrays.Enqueue(new object[length]);
        }

        public static T GetValue<T>(this object[] array, int pos)
        {
            if (pos < 0)
                throw new IndexOutOfRangeException("Array index can't be less then zero");

            if (array == null)
                throw new NullReferenceException("Attempted to access a object array but it's null");

            if (pos > array.Length)
                throw new IndexOutOfRangeException("Array index is greater then array length");

            var obj = array[pos];

            if (obj == null)
                return default(T);

            if (obj is T same)
                return same;

            try
            {
                same = (T) Convert.ChangeType(obj, typeof(T));
                return same;
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static bool IsArraySingleValue(object value, object[] array)
        {
            return array != null && array.All(o => o == value);
        }
    }
}
