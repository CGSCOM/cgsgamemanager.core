﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CGSGameManager.Core.Configuration
{
    public sealed class JsonConfig : ConfigFile
    {
        private readonly bool _saveAfterChange;
        private JObject data;

        private JsonConfig(string fileName, bool saveAfterChange = false) : base(fileName)
        {
            _saveAfterChange = saveAfterChange;
        }

        protected override string Extension => ".json";

        public object this[string path]
        {
            set => WriteObject(value, path?.Split('.', '/'));
        }

        public void WriteObject<T>(T obj, params string[] path)
        {
            if (path == null || path?.Length == 0)
            {
                data.Add(obj);

                if (_saveAfterChange)
                    Save();
                return;
            }

            var valueKey = path.Last();
            var pv = new string[path.Length - 1];
            for (var i = 0; i < pv.Length; i++)
                pv[i] = path[i];

            JToken item = data;

            for (var i = 0; i < pv.Length; i++)
            {
                var key = pv[i];
                var current = item[key];

                if (current == null)
                {
                    current = item[key] = new JObject();
                }
                else if (current is JArray || current is JProperty || current is JValue)
                {
                    current.Remove();
                    current = item[key] = new JObject();
                }

                item = current;
            }

            if (typeof(T).IsArray)
            {
                var jarray = (JArray) (item[valueKey] = new JArray());
                var list = obj as object[];

                foreach (var v in list) jarray.Add(v);

                if (_saveAfterChange)
                    Save();
                return;
            }

            if (typeof(T).GetInterface(nameof(ICollection)) != null)
            {
                var j = default(JToken);
                var list = obj as ICollection;

                string type = null;
                var kvp = typeof(KeyValuePair<,>);
                foreach (var v in list)
                {
                    if (type == null)
                    {
                        if (v.GetType().GetGenericTypeDefinition() == kvp)
                        {
                            type = "dict";
                            j = item[valueKey] = new JObject();
                        }
                        else
                        {
                            type = "list";
                            j = item[valueKey] = new JArray();
                        }
                    }

                    switch (type)
                    {
                        case "dict":
                        {
                            var key = v.GetType().GetProperty("Key")?.GetValue(v, null);
                            var value = v.GetType().GetProperty("Value")?.GetValue(v, null);
                            var jo = j as JObject;
                            jo[key.ToString()] = new JValue(value);
                        }
                            continue;

                        case "list":
                        {
                            var ja = j as JArray;
                            ja.Add(v);
                        }
                            continue;

                        default:
                            throw new InvalidOperationException(type);
                    }
                }

                if (_saveAfterChange)
                    Save();
                return;
            }

            if (_saveAfterChange)
                Save();
            item[valueKey] = new JValue(obj);
        }

        public T ReadObject<T>(params string[] path)
        {
            if (path == null || path?.Length == 0) return data.ToObject<T>();

            var v = data.SelectToken(string.Join(".", path), false);

            return v != null ? v.ToObject<T>() : default(T);
        }

        public override void Save()
        {
            StreamWriter stream = null;
            try
            {
                var file = new FileInfo(FullName);

                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();

                if (!data.HasValues)
                    return;

                var content = data.ToString(Formatting.Indented);

                if (string.IsNullOrEmpty(content))
                    return;

                stream = new StreamWriter(file.Open(FileMode.Create, FileAccess.ReadWrite));

                var array = content.ToCharArray();

                stream.Write(array, 0, array.Length);
            }
            catch (Exception e)
            {
                Interface.Core.LogException($"Failed to save config file {BaseFileName}", e);
            }
            finally
            {
                if (stream != null)
                {
                    stream.Flush();
                    stream.Close();
                    stream.Dispose();
                }
            }
        }

        public override void Load()
        {
            StreamReader stream = null;
            try
            {
                var file = new FileInfo(FullName);

                if (!file.Exists)
                    return;

                if (file.Directory != null && !file.Directory.Exists)
                    file.Directory.Create();

                stream = new StreamReader(file.Open(FileMode.Open, FileAccess.Read));

                var str = stream.ReadToEnd();
                data = JObject.Parse(str);
            }
            catch (Exception e)
            {
                Interface.Core.LogException($"Failed to load config file {BaseFileName}", e);
                data = new JObject();
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                }
            }
        }

        public static JsonConfig LoadOrCreate(string filename)
        {
            var config = Load(filename) ?? Create(filename);

            return config;
        }

        public static JsonConfig Load(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                throw new IOException("Attempted to load a file with no name");

            if (!filename.EndsWith(".json"))
                throw new IOException($"FileName {filename} needs to end with .json");

            if (!File.Exists(filename))
                return null;

            var config = new JsonConfig(filename);
            config.Load();

            return config;
        }

        public static JsonConfig Create(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                throw new IOException("Attempted to load a file with no name");

            if (!filename.EndsWith(".json"))
                throw new IOException($"FileName {filename} needs to end with .json");

            var config = new JsonConfig(filename) {data = new JObject()};
            return config;
        }
    }
}
