﻿using System;
using System.IO;
using System.Linq;

namespace CGSGameManager.Core.Configuration
{
    public abstract class ConfigFile
    {
        protected readonly string BaseDirectory;
        protected readonly string BaseFileName;

        protected ConfigFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException(nameof(fileName), "Filename can't be null or empty");

            fileName = fileName.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar).Replace(
                $"{Path.DirectorySeparatorChar}{Path.DirectorySeparatorChar}", Path.DirectorySeparatorChar.ToString());
            BaseDirectory = string.Empty;
            if (fileName.Contains(Path.DirectorySeparatorChar.ToString()))
            {
                var split = fileName.Split(new char[1] {Path.DirectorySeparatorChar},
                    StringSplitOptions.RemoveEmptyEntries);

                fileName = split.Last();

                for (var i = 0; i < split.Length - 1; i++)
                {
                    if (split[i].Contains(":"))
                        continue;

                    BaseDirectory += split[i] + Path.DirectorySeparatorChar;
                }
            }

            if (fileName.Contains('.'))
            {
                var index = fileName.IndexOf('.');
                fileName = fileName.Remove(index, fileName.Length - index);
            }

            BaseFileName = fileName;
        }

        protected abstract string Extension { get; }

        public string FullName => BuildPath();

        public virtual void Save()
        {
            throw new NotImplementedException("Save has not been implemented yet");
        }

        public virtual void Load()
        {
            throw new NotImplementedException("Load has not been implemented yet");
        }

        protected string BuildPath()
        {
            var directory = string.IsNullOrEmpty(BaseDirectory)
                ? Interface.Core.ConfigDirectory.FullName
                : Path.Combine(Interface.Core.ConfigDirectory.FullName, BaseDirectory);

            var filePath = string.IsNullOrEmpty(Extension)
                ? Path.Combine(directory, BaseFileName)
                : Path.Combine(directory, BaseFileName + Extension);
            return filePath;
        }
    }
}
