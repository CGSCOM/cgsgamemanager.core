﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CGSGameManager
{
    public static class Utility
    {
        #region IP Address Conversion

        public static IPAddress GetIPAddress(uint IPAddress)
        {
            if (IPAddress == default(uint)) return null;

            return new IPAddress(IPAddress);
        }

        #endregion

        #region String Extensions

        public static bool IsNullOrWhiteSpace(this string str)
        {
            if (str == null)
                return true;

            foreach (var c in str)
                if (!char.IsWhiteSpace(c))
                    return false;

            return true;
        }

        public static bool IsValidJson(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            value = value.Trim();

            if (!(value.StartsWith("{") && value.EndsWith("}")) &&
                !(value.StartsWith("[") && value.EndsWith("]"))) return false;

            try
            {
                var obj = JToken.Parse(value);
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
            catch (Exception ex)
            {
                Interface.Core.LogException("Failed to validate Json Content", ex);
                return false;
            }
        }

        public static bool IsValidXml(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            value = value.Trim();
            if (!value.StartsWith("<") && !value.EndsWith(">"))
                return false;
            var xml = new XmlDocument();
            try
            {
                xml.LoadXml(value);
                return true;
            }
            catch (XmlException)
            {
                return false;
            }
            catch (Exception ex)
            {
                Interface.Core.LogException("Failed to validate Xml Document", ex);
                return false;
            }
        }

        #endregion

        #region Clock Management

        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        ///     Get the Current Unix Timestamp
        /// </summary>
        /// <returns>Unix TimeStamp</returns>
        public static long GetUnixTime()
        {
            return (long) (DateTime.UtcNow - Epoch).TotalSeconds;
        }

        /// <summary>
        ///     Convert a Unix Timestamp into a DateTime
        /// </summary>
        /// <param name="unixTime">Seconds Since Epoch</param>
        /// <returns>DateTime of UnixTime</returns>
        public static DateTime FromEpoch(long unixTime)
        {
            return Epoch.AddSeconds(unixTime);
        }

        #endregion

        #region Reflections

        internal static string GetModulePath(Assembly assembly)
        {
            if (assembly == null)
                return null;

            var codebase = assembly.CodeBase;
            var uri = new UriBuilder(codebase);
            var path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        internal static bool SetReference(MemberInfo info, object instance, object value)
        {
            if (info == null || instance == null)
                return false;

            if (info is PropertyInfo property)
            {
                if (!property.CanWrite)
                    return false;

                try
                {
                    property.SetValue(instance, value, null);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            if (!(info is FieldInfo field))
                return false;

            if (field.IsInitOnly)
                return false;

            try
            {
                field.SetValue(instance, value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal static IEnumerable<MemberInfo> GetMembersFromType(object source, Type assignableType)
        {
            if (source == null)
                yield break;

            // Grab Fields
            foreach (var field in source.GetType()
                .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.IsInitOnly || field.IsLiteral)
                    continue;

                if (assignableType != null)
                    if (!assignableType.IsAssignableFrom(field.FieldType))
                        continue;

                yield return field;
            }

            foreach (var property in source.GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (!property.CanWrite)
                    continue;

                if (assignableType != null)
                    if (!assignableType.IsAssignableFrom(property.PropertyType))
                        continue;

                yield return property;
            }
        }

        #endregion
    }
}
