﻿using System;
using System.Reflection;

namespace CGSGameManager.Core
{
    public struct VersionNumber
    {
        public readonly int Major;
        public readonly int Minor;
        public readonly int Build;

        public VersionNumber(Version version) : this(version.Major, version.Minor, version.Build)
        {
        }

        public VersionNumber(AssemblyName assembly) : this(assembly.Version)
        {
        }

        public VersionNumber(int major, int minor, int build)
        {
            Major = major;
            Minor = minor;
            Build = build;
        }

        public static bool operator ==(VersionNumber version1, VersionNumber version2)
        {
            return version1.Major == version2.Major && version1.Minor == version2.Minor &&
                   version1.Build == version2.Build;
        }

        public static bool operator !=(VersionNumber version1, VersionNumber version2)
        {
            return !(version1 == version2);
        }

        public override string ToString()
        {
            return $"{Major}.{Minor}.{Build}";
        }

        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            return (Major << 1) | (Minor << 5) | (Build << 10);
        }

        public static VersionNumber CgsCurrent()
        {
            return new VersionNumber(Assembly.GetAssembly(typeof(CGSManager)).GetName());
        }
    }
}
