﻿using CGSGameManager.Core;
using CGSGameManager.Core.Logging;

namespace CGSGameManager
{
    public delegate void DebugCallback(Logger.LogMessage msg);

    public static class Interface
    {
        public static CGSManager Core { get; private set; }

        public static void Initialize() => Initialize(null);

        public static void Initialize(DebugCallback callback)
        {
            if (Core != null) return;
            Core = new CGSManager();
            Core.Load(callback);
        }

        public static void Shutdown()
        {
            if (Core == null) return;
            Core.Shutdown();
            Core = null;
        }

        #region Hook Calling

        private static object CallHook(string hookname, object[] args)
        {
            return Core?.ExtensionManager?.CallHook(hookname, args);
        }

        public static object CallHook(string hook)
        {
            return CallHook(hook, null);
        }

        public static object CallHook(string hook, object obj1)
        {
            var array = ArrayPool.Get(1);
            array[0] = obj1;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2)
        {
            var array = ArrayPool.Get(2);
            array[0] = obj1;
            array[1] = obj2;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3)
        {
            var array = ArrayPool.Get(3);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4)
        {
            var array = ArrayPool.Get(4);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5)
        {
            var array = ArrayPool.Get(5);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
            object obj6)
        {
            var array = ArrayPool.Get(6);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
            object obj6, object obj7)
        {
            var array = ArrayPool.Get(7);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            array[6] = obj7;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
            object obj6, object obj7, object obj8)
        {
            var array = ArrayPool.Get(8);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            array[6] = obj7;
            array[7] = obj8;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
            object obj6, object obj7, object obj8, object obj9)
        {
            var array = ArrayPool.Get(9);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            array[6] = obj7;
            array[7] = obj8;
            array[8] = obj9;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        public static object CallHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
            object obj6, object obj7, object obj8, object obj9, object obj10)
        {
            var array = ArrayPool.Get(10);
            array[0] = obj1;
            array[1] = obj2;
            array[2] = obj3;
            array[3] = obj4;
            array[4] = obj5;
            array[5] = obj6;
            array[6] = obj7;
            array[7] = obj8;
            array[8] = obj9;
            array[9] = obj10;
            var ret = CallHook(hook, array);
            ArrayPool.Free(array);
            return ret;
        }

        #endregion

        //#region Oxide

        //public static object CallOxideHook(string hook, object[] args)
        //{
        //    return Oxide.Core.Interface.CallHook(hook, args);
        //}

        //public static object CallOxideHook(string hook)
        //{
        //    return CallOxideHook(hook, null);
        //}

        //public static object CallOxideHook(string hook, object obj1)
        //{
        //    var array = ArrayPool.Get(1);
        //    array[0] = obj1;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2)
        //{
        //    var array = ArrayPool.Get(2);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3)
        //{
        //    var array = ArrayPool.Get(3);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4)
        //{
        //    var array = ArrayPool.Get(4);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5)
        //{
        //    var array = ArrayPool.Get(5);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
        //    object obj6)
        //{
        //    var array = ArrayPool.Get(6);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    array[5] = obj6;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
        //    object obj6, object obj7)
        //{
        //    var array = ArrayPool.Get(7);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    array[5] = obj6;
        //    array[6] = obj7;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
        //    object obj6, object obj7, object obj8)
        //{
        //    var array = ArrayPool.Get(8);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    array[5] = obj6;
        //    array[6] = obj7;
        //    array[7] = obj8;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
        //    object obj6, object obj7, object obj8, object obj9)
        //{
        //    var array = ArrayPool.Get(9);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    array[5] = obj6;
        //    array[6] = obj7;
        //    array[7] = obj8;
        //    array[8] = obj9;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //public static object CallOxideHook(string hook, object obj1, object obj2, object obj3, object obj4, object obj5,
        //    object obj6, object obj7, object obj8, object obj9, object obj10)
        //{
        //    var array = ArrayPool.Get(10);
        //    array[0] = obj1;
        //    array[1] = obj2;
        //    array[2] = obj3;
        //    array[3] = obj4;
        //    array[4] = obj5;
        //    array[5] = obj6;
        //    array[6] = obj7;
        //    array[7] = obj8;
        //    array[8] = obj9;
        //    array[9] = obj10;
        //    var ret = CallOxideHook(hook, array);
        //    ArrayPool.Free(array);
        //    return ret;
        //}

        //#endregion
    }
}
