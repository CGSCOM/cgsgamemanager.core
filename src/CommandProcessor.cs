﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CGSGameManager.Core
{
    public class CommandProcessor : IDisposable
    {
        private FileSystemWatcher _ConfigWatcher;
        private List<BaseCommand> _RegisteredCommands;
        private bool Loaded;

        public CommandProcessor()
        {
            _RegisteredCommands = new List<BaseCommand>();
            _ConfigWatcher = new FileSystemWatcher(Interface.Core.ConfigDirectory.FullName, "*.cfg")
            {
                EnableRaisingEvents = false,
                IncludeSubdirectories = false,
                NotifyFilter = NotifyFilters.LastWrite
            };

            _ConfigWatcher.Created += (o, s) => ReadConfig(s.Name, s.FullPath);
            _ConfigWatcher.Changed += (o, s) => ReadConfig(s.Name, s.FullPath);
        }

        public void Dispose()
        {
            _ConfigWatcher.Dispose();
            _RegisteredCommands.Clear();
            _ConfigWatcher = null;
            _RegisteredCommands = null;
        }

        /// <summary>
        ///     Does the Initial Config readyness steps and loads all the current config files then enables the FileSystemWactcher
        /// </summary>
        public void ReadConfigs()
        {
            if (Loaded) return;
            Loaded = true;
            var files = Directory.GetFiles(Interface.Core.ConfigDirectory.FullName, "*.cfg",
                SearchOption.TopDirectoryOnly);

            foreach (var file in files) ReadConfig(Path.GetFileName(file), file);
            _ConfigWatcher.EnableRaisingEvents = true;
        }

        private void ReadConfig(string Filename, string FullName)
        {
            if (!File.Exists(FullName)) return;

            var cfg = default(string[]);

            try
            {
                cfg = File.ReadAllLines(FullName);
            }
            catch (Exception ex)
            {
                Interface.Core.LogException($"[CommandProcessor] Failed to read cfg file {Filename} - {ex.Message}",
                    ex);
                return;
            }

            if (cfg == null || (cfg?.Length ?? 0) == 0)
            {
                Interface.Core.LogInfo($"[CommandProcessor] Attempted to read cfg file {Filename} but file is empty");
                return;
            }

            var commands = 0;
            var failed = 0;

            for (var i = 0; i < cfg.Length; i++)
            {
                if (cfg[i].StartsWith("#")) continue;
                commands++;
                var cmdarray = cfg[i].Split(' ').ToArray();

                if (cmdarray.Length == 1)
                {
                    if (!ProcessCommand(cmdarray[0], new string[0])) failed++;
                    continue;
                }

                var args = cmdarray.Skip(1).ToArray();

                if (!ProcessCommand(cmdarray[0], args)) failed++;
            }

            Interface.Core.LogInfo(
                $"[CommandProcessor] Successfully processed cfg file {Filename} with {commands} total commands{(failed > 0 ? $" and {failed} of them failed to execute" : null)}");
        }

        /// <summary>
        ///     Registers a new command with the command processor
        /// </summary>
        /// <param name="command"></param>
        /// <param name="callback"></param>
        /// <param name="help"></param>
        public void RegisterCommand(string command, Action<string[]> callback, string help)
        {
            if (string.IsNullOrEmpty(command))
            {
                Interface.Core.LogError("[CommandProcessor] Failed to register command because command was null");
                return;
            }

            if (callback == null)
            {
                Interface.Core.LogError(
                    $"[CommandProcessor] Failed to register command {command} because callback was not set");
                return;
            }

            try
            {
                var cmd = new BaseCommand(command, callback, help);

                RegisterCommand(cmd);
            }
            catch (Exception ex)
            {
                Interface.Core.LogException($"[CommandProcessor] Failed to register command {command} - {ex.Message}",
                    ex);
            }
        }

        /// <summary>
        ///     Registers a new command with the command processor
        /// </summary>
        /// <param name="command"></param>
        public void RegisterCommand(BaseCommand command)
        {
            if (_RegisteredCommands.Contains(command))
            {
                Interface.Core.LogError(
                    $"[CommandProcessor] Failed to register command {command.Fullname} because command is already registered");
                return;
            }

            _RegisteredCommands.Add(command);
            if (CGSManager.DEBUGMODE)
                Interface.Core.LogInfo($"[CommandProcessor] Command {command.Fullname} was registered successfully");
        }

        /// <summary>
        ///     Attempts to execute command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public bool ProcessCommand(string command, string[] args)
        {
            if (_RegisteredCommands == null)
            {
                Interface.Core.LogError(
                    $"[CommandProcessor] Failed to run command '{command}' Command Library isn't loaded");
                return false;
            }

            IEnumerable<BaseCommand> commands = null;

            if (command.Contains('.'))
                commands = _RegisteredCommands.Where(a => a.Fullname == command.ToLower());
            else
                commands = _RegisteredCommands.Where(a => a.Name == command.ToLower());

            if (commands == null || (commands?.Count() ?? 0) == 0)
            {
                Interface.Core.LogError($"[CommandProcessor] Failed to run command '{command}' Command not found");
                return false;
            }

            if (commands.Count() > 1)
            {
                var cmdstr = string.Join(", ", commands.Select(a => a.Fullname).ToArray());
                Interface.Core.LogError(
                    $"[CommandProcessor] Failed to execute command '{command}' Multiple commands found please use full name - {cmdstr}");
                return false;
            }

            var cmd = commands.FirstOrDefault();
            if (args == null) args = new string[0];

            try
            {
                cmd.Run(args);
                return true;
            }
            catch (Exception ex)
            {
                Interface.Core.LogException($"[CommandProcessor] Failed to execute command '{command}' - {ex.Message}",
                    ex);
                return false;
            }
        }

        internal void Help()
        {
            var sb = new StringBuilder();
            foreach (var h in _RegisteredCommands) sb.AppendLine(h.ToString());

            Interface.Core.LogInfo(sb.ToString());
        }
    }

    public class BaseCommand
    {
        public BaseCommand(string name, Action<string[]> callback, string help = null)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name", "A Required parameter was not set");
            Callback = callback ?? throw new ArgumentNullException("callback", "A Required parameter was not set");
            name = name.Replace(" ", "");

            if (name.Contains('.'))
            {
                var index = name.IndexOf('.');
                var b = name.Substring(0, index).ToLower();
                var c = name.Substring(++index, name.Length - index).ToLower();

                switch (b)
                {
                    case "cgs":
                    case "oxide":
                    case "unity":
                    case "game":
                    case "system":
                    case "eac":
                    case "steam":
                        break;

                    default:
                        throw new InvalidOperationException($"Invalid command base {b}");
                }

                Fullname = $"{b}.{c}";
                Name = c;
            }
            else
            {
                Name = name.ToLower();
                Fullname = $"cgs.{name.ToLower()}";
            }

            if (!string.IsNullOrEmpty(help)) Help = help;
        }

        protected Action<string[]> Callback { get; set; }
        public string Fullname { get; protected set; }
        public string Name { get; protected set; }
        public string Help { get; protected set; }

        public virtual void Run(string[] args)
        {
            Callback?.Invoke(args);
        }

        public override string ToString()
        {
            return $"Command: {Fullname} - Help: {Help}";
        }

        public override bool Equals(object obj)
        {
            return obj is BaseCommand && (obj as BaseCommand).Fullname == Fullname;
        }

        public override int GetHashCode()
        {
            return Fullname.GetHashCode();
        }
    }
}
