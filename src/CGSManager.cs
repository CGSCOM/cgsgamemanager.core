﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Covalence;
using CGSGameManager.Core.Extensions;
using CGSGameManager.Core.Logging;
using System.Net;

namespace CGSGameManager.Core
{
    public sealed class CGSManager
    {
        internal CGSManager()
        {
            _gameThread = Thread.CurrentThread;
            _nextTickQueue = new Queue<Action>();
            var core = Assembly.GetAssembly(typeof(CGSManager));
            AssemblyDirectory = new DirectoryInfo(Utility.GetModulePath(core));
            if (!AssemblyDirectory.Exists)
            {
                throw new DirectoryNotFoundException("Unable to find Core Module Directory");
            }

            var root = Environment.CurrentDirectory;
            if (root.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
                root = AppDomain.CurrentDomain.BaseDirectory;

            if (string.IsNullOrEmpty(root))
            {
                throw new DirectoryNotFoundException("Root Directory is null");
            }

            RootDirectory = new DirectoryInfo(root);

            var instance = CommandLine.GetSwitch("cgsdir", null);
            InstanceDirectory = string.IsNullOrEmpty(instance)
                ? new DirectoryInfo(Path.Combine(RootDirectory.FullName, @"CGS\"))
                : new DirectoryInfo(instance);

            var instanceDirectory = InstanceDirectory.FullName;
            LogDirectory = new DirectoryInfo(Path.Combine(instanceDirectory, @"Logs\"));
            ConfigDirectory = new DirectoryInfo(Path.Combine(instanceDirectory, @"Config\"));
            DataDirectory = new DirectoryInfo(Path.Combine(instanceDirectory, @"Data\"));
            ExtensionDirectory = new DirectoryInfo(Path.Combine(instanceDirectory, @"Extensions\"));

#if DEBUG
            DEBUGMODE = true;
#else
            DEBUGMODE = false || CommandLine.HasSwitch("cgs-debug");
#endif
        }

        public static bool DEBUGMODE { get; internal set; }

        public CommandProcessor CommandProcessor { get; private set; }

        #region Extensions

        public ExtensionManager ExtensionManager { get; private set; }

        #endregion

        #region Covalence

        public ICovalenceProvider CovalenceProvider { get; private set; }

        public TCovalenceProvider RegisterCovalence<TCovalenceProvider>() where TCovalenceProvider : ICovalenceProvider, new()
        {
            if (CovalenceProvider != null) return default(TCovalenceProvider);
            var covalence = new TCovalenceProvider();
            CovalenceProvider = covalence;
            return covalence;
        }

        #endregion

        #region Logging

        public CompoundLogger CompoundLogger { get; private set; }

        public void LogInfo(string message, params object[] args)
        {
            CompoundLogger.Write(LogType.Info, message, args);
        }

        public void LogWarning(string message, params object[] args)
        {
            CompoundLogger.Write(LogType.Warning, message, args);
        }

        public void LogError(string message, params object[] args)
        {
            CompoundLogger.Write(LogType.Error, message, args);
        }

        public void LogException(string message, Exception ex)
        {
            CompoundLogger.WriteException(message, ex);
        }

        #endregion

        #region Threading

        private readonly Thread _gameThread;

        private readonly Queue<Action> _nextTickQueue;

        private bool _nextTickRegistered = false;

        public Action RegisterTickCallback()
        {
            if (_nextTickRegistered)
                return null;

            _nextTickRegistered = true;
            return OnTick;
        }

        private void OnTick()
        {
            Action item = null;
            lock (_nextTickQueue)
            {
                if (_nextTickQueue.Count == 0)
                    return;

                item = _nextTickQueue.Dequeue();
            }

            try
            {
                item();
            }
            catch (Exception e)
            {
                Interface.Core.LogException("Failed to process NextTick Action", e);
            }
        }

        public bool IsMainThread()
        {
            return Thread.CurrentThread == _gameThread;
        }

        public void NextFrame(Action action)
        {
            if (action == null)
                return;
            lock (_nextTickQueue)
            {
                _nextTickQueue.Enqueue(action);
            }
        }

        public bool QueueAsyncAction(WaitCallback callback, object state)
        {
            if (callback != null) return ThreadPool.QueueUserWorkItem(callback, state);
            LogError("Unable to process AsyncCallback: Callback is null");
            return false;
        }

        public bool QueueAsyncAction(Action callback)
        {
            if (callback != null) return QueueAsyncAction(HandleAsyncActionCallback, callback);
            LogError("Unable to process AsyncCallback: Callback is null");
            return false;
        }

        private void HandleAsyncActionCallback(object state)
        {
            switch (state)
            {
                case null:
                    return;
                case Action action:
                    try
                    {
                        action();
                    }
                    catch (Exception ex)
                    {
                        LogException("Failed to process Async Action",
                            ex is TargetInvocationException && ex.InnerException != null ? ex.InnerException : ex);
                    }

                    break;
            }
        }

        #endregion

        #region Directories

        public readonly DirectoryInfo RootDirectory;

        public readonly DirectoryInfo AssemblyDirectory;

        public readonly DirectoryInfo InstanceDirectory;

        public readonly DirectoryInfo ExtensionDirectory;

        public readonly DirectoryInfo LogDirectory;

        public readonly DirectoryInfo ConfigDirectory;

        public readonly DirectoryInfo DataDirectory;

        private void CreateDirectories()
        {
            if (!InstanceDirectory.Exists) InstanceDirectory.Create();
            if (!ConfigDirectory.Exists) ConfigDirectory.Create();
            if (!LogDirectory.Exists) LogDirectory.Create();
            if (!DataDirectory.Exists) DataDirectory.Create();
            if (!ExtensionDirectory.Exists) ExtensionDirectory.Create();
            MigrateExtensions();
        }

        private void MigrateExtensions()
        {
            var files = AssemblyDirectory.GetFiles("CGSGameManager.*.dll", SearchOption.TopDirectoryOnly);

            if (files.Length == 0)
                return;

            foreach (var file in files)
            {
                if (file.Name.Equals("CGSGameManager.Core.dll")) continue;
                var newfile = new FileInfo(Path.Combine(ExtensionDirectory.FullName, file.Name));

                try
                {
                    if (newfile.Exists) newfile.Delete();

                    file.CopyTo(newfile.FullName);
                    file.Delete();
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region Startup

        public bool ShutdownStarted { get; private set; }

        internal void Load(DebugCallback callback = null)
        {
            CreateDirectories();

            CompoundLogger = new CompoundLogger(callback);
            CompoundLogger.RegisterLogger(new RotatingFileLogger(LogDirectory.FullName));

            CommandProcessor = new CommandProcessor();
            ExtensionManager = new ExtensionManager();
            var core = new CoreExtension(ExtensionManager);
            var extentions = ExtensionManager
                .GetType()
                .GetField("_loadedExtensions", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(ExtensionManager) as Hash<string, Extension>;
            extentions.Add(nameof(CoreExtension), core);
            core.InternalInit();
            ExtensionManager.LoadAllAssemblies();
            CommandProcessor.ReadConfigs();

            try
            {
                var key = File.ReadAllText(Path.Combine(RootDirectory.FullName, "license.txt"));
                using (var client = new WebClient())
                {
                    client.BaseAddress = "https://crackedgameservers.com/api/licensemanager/license/?key=2e181a581b4eaf6a32016e2c61e918da";
                    client.DownloadString($"&licenseKey={key}&serverPort={CovalenceProvider.GetServer().Endpoint.Port}&game={CovalenceProvider.GameName}");
                }
            }
            catch (Exception ex)
            {
                LogException("Failed to verify license", ex);
                Shutdown();
            }
        }

        internal void Shutdown()
        {
            if (ShutdownStarted) return;
            ShutdownStarted = true;
            CovalenceProvider?.Dispose();
            CovalenceProvider = null;
            ExtensionManager.Shutdown();
            CompoundLogger.OnRemoved();
        }

        #endregion
    }
}
