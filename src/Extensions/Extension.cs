﻿using System;
using System.Linq;
using System.Reflection;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Libraries;
using CGSGameManager.Core.Plugins;

namespace CGSGameManager.Core.Extensions
{
    public abstract class Extension
    {
        #region References

        protected ExtensionManager Manager { get; }

        protected CGSManager Core => Interface.Core;

        #endregion

        #region Collections

        internal readonly Hash<string, Library> LibraryCollection;
        internal readonly Hash<string, Plugin> PluginCollection;

        #endregion

        #region Information

        /// <summary>
        ///     Extension Name
        /// </summary>
        public virtual string ExtensionName => GetType().Name;

        /// <remarks>Can be set with a override or via the AssemblyCompanyAttribute</remarks>
        /// <summary>
        ///     Extension Author
        /// </summary>
        public virtual string ExtensionAuthor =>
            (Assembly.GetAssembly(GetType()).GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                .FirstOrDefault() as AssemblyCompanyAttribute)?.Company;

        /// <summary>
        ///     Extension Version Number
        /// </summary>
        public virtual VersionNumber ExtensionVersion => new VersionNumber(Assembly.GetAssembly(GetType()).GetName());

        /// <summary>
        ///     Identifies this extension as a Game Extension
        /// </summary>
        public bool IsGameExtension { get; internal set; }

        #endregion

        #region Initialization

        protected Extension(ExtensionManager manager)
        {
            Manager = manager;
            LibraryCollection = new Hash<string, Library>();
            PluginCollection = new Hash<string, Plugin>();
        }

        /// <summary>
        ///     Called when this extension is being loaded by the <see cref="ExtensionManager" />
        /// </summary>
        protected virtual void Load()
        {
        }

        /// <summary>
        ///     Called when this extension is being unloaded by the <see cref="ExtensionManager" />
        /// </summary>
        protected virtual void Unload()
        {
        }

        /// <summary>
        ///     <para>Called when the GameServer has been fully initialized.</para>
        ///     <para>Generally when the server starts accepting players</para>
        /// </summary>
        protected virtual void ServerInitialized()
        {
        }

        internal void InternalUnload()
        {
            lock (LibraryCollection)
            {
                foreach (var library in LibraryCollection.Values.ToList()) UnregisterLibrary(library);
            }

            lock (PluginCollection)
            {
                foreach (var plugin in PluginCollection.Values.ToList()) UnregisterPlugin(plugin);
            }

            Unload();
        }

        internal void InternalInit()
        {
            Load();
        }

        internal void ServerInitCompleted()
        {
            try
            {
                ServerInitialized();
            }
            catch (Exception e)
            {
                LogException("Failed to ServerInitialized", e);
            }
        }

        #endregion

        #region Libraries

        /// <summary>
        ///     Registers a <see cref="Library" /> with this <seealso cref="Extension" /> and the
        ///     <seealso cref="ExtensionManager" />
        /// </summary>
        /// <param name="name">Name override for the <see cref="Library" /> (Default: ClassName)</param>
        /// <param name="library">A <see cref="Library" /> instance</param>
        protected void RegisterLibrary(string name, Library library)
        {
            if (library == null)
                return;

            if (string.IsNullOrEmpty(name))
                name = library.GetType().Name;

            if (Manager.IsLibraryNameTaken(name))
            {
                LogError($"Attempted to register a library with the same name as a existing library '{name}'");
                return;
            }

            lock (LibraryCollection)
            {
                LibraryCollection.Add(name, library);
            }

            ExtensionManager.OnLibraryRegistered?.Invoke(library);
            library.InternalLoad(name);

            LogInfo($"Registered Library {name}");
        }

        /// <summary>
        ///     Unregisters <see cref="Library" /> loaded by this <seealso cref="Extension" />
        /// </summary>
        /// <param name="library">The <see cref="Library" /> instance to unload</param>
        /// <returns>True if Success</returns>
        protected bool UnregisterLibrary(Library library)
        {
            if (library == null)
                return false;

            var type = library.GetType();
            var removed = false;
            var name = string.Empty;
            lock (LibraryCollection)
            {
                foreach (var libpair in LibraryCollection.ToList())
                {
                    if (!type.IsInstanceOfType(libpair.Value))
                        continue;

                    removed = LibraryCollection.Remove(libpair);
                    name = libpair.Key;
                    break;
                }
            }

            if (!removed) return false;

            ExtensionManager.OnLibraryUnregistered?.Invoke(library);
            library.InternalUnload();
            LogInfo($"Unregistered Library {name}");

            return true;
        }

        /// <summary>
        ///     Unregisters <see cref="Library" /> loaded by this <seealso cref="Extension" /> by Name
        /// </summary>
        /// <param name="name">The <see cref="Library" /> Name</param>
        /// <returns>True if Success</returns>
        protected bool UnregisterLibrary(string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            var removed = false;
            var lib = default(Library);

            lock (LibraryCollection)
            {
                foreach (var libpair in LibraryCollection.ToList())
                {
                    if (!string.Equals(libpair.Key, name, StringComparison.OrdinalIgnoreCase))
                        continue;

                    lib = libpair.Value;
                    name = libpair.Key;
                    removed = LibraryCollection.Remove(libpair.Key);
                }
            }

            if (!removed) return false;

            ExtensionManager.OnLibraryUnregistered?.Invoke(lib);
            lib.InternalUnload();
            LogInfo($"Unregistered Library {name}");
            return true;
        }

        #endregion

        #region Plugins

        private readonly PropertyInfo _pluginName = typeof(Plugin).GetProperty("Name");
        private readonly PropertyInfo _pluginVersion = typeof(Plugin).GetProperty("Version");

        /// <summary>
        ///     Registers a <see cref="Plugin" /> with this <seealso cref="Extension" /> and the
        ///     <seealso cref="ExtensionManager" />
        /// </summary>
        /// <param name="name">Name override for the <see cref="Plugin" /> (Default: ClassName)</param>
        /// <param name="plugin">A <see cref="Plugin" /> instance</param>
        protected void RegisterPlugin(string name, Plugin plugin)
        {
            if (plugin == null)
                return;

            plugin.Extension = this;
            if (string.IsNullOrEmpty(name))
                name = plugin.Name;

            if (string.IsNullOrEmpty(name))
                name = plugin.GetType().Name;

            if (plugin.Name != name)
                _pluginName.SetValue(plugin, name, null);

            if (plugin.Version == default(VersionNumber))
                _pluginVersion.SetValue(plugin, new VersionNumber(plugin.ReferencingAssembly), null);

            if (Manager.IsPluginNameTaken(name))
            {
                LogError($"Attempted to register a plugin with the same name as a existing plugin '{name}'");
                return;
            }

            lock (PluginCollection)
            {
                PluginCollection.Add(name, plugin);
            }

            plugin.Load();

            ExtensionManager.OnPluginRegistered?.Invoke(plugin);

            plugin.Call("Loaded", null);

            LogInfo($"Registered Plugin {name} v{plugin.Version}");
        }

        /// <summary>
        ///     Unregisters <see cref="Plugin" /> loaded by this <seealso cref="Extension" />
        /// </summary>
        /// <param name="plugin">The <see cref="Plugin" /> instance to unload</param>
        /// <returns>True if Success</returns>
        protected bool UnregisterPlugin(Plugin plugin)
        {
            if (plugin == null)
                return false;

            var type = plugin.GetType();
            var removed = false;
            var name = string.Empty;
            lock (PluginCollection)
            {
                foreach (var pluginPair in PluginCollection.ToList())
                {
                    if (!type.IsInstanceOfType(pluginPair.Value))
                        continue;

                    removed = PluginCollection.Remove(pluginPair);
                    name = pluginPair.Key;
                    break;
                }
            }

            if (!removed) return false;

            plugin.OnRemoved();
            ExtensionManager.OnPluginUnregistered?.Invoke(plugin);
            LogInfo($"Unregistered Plugin {name}");

            return true;
        }

        /// <summary>
        ///     Unregisters <see cref="Plugin" /> loaded by this <seealso cref="Extension" /> by Name
        /// </summary>
        /// <param name="name">The <see cref="Plugin" /> Name</param>
        /// <returns>True if Success</returns>
        protected bool UnregisterPlugin(string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            var removed = false;
            var plugin = default(Plugin);

            lock (PluginCollection)
            {
                foreach (var pluginpair in PluginCollection.ToList())
                {
                    if (!string.Equals(pluginpair.Key, name, StringComparison.CurrentCultureIgnoreCase))
                        continue;

                    plugin = pluginpair.Value;
                    name = pluginpair.Key;
                    removed = PluginCollection.Remove(pluginpair.Key);
                }
            }

            if (!removed) return false;

            plugin.OnRemoved();
            ExtensionManager.OnPluginUnregistered?.Invoke(plugin);
            LogInfo($"Unregistered Plugin {name}");
            return true;
        }

        #endregion

        #region Logging

        protected void LogInfo(string message)
        {
            Core.LogInfo($"[{ExtensionName}] {message}");
        }

        protected void LogWarning(string message)
        {
            Core.LogWarning($"[{ExtensionName}] {message}");
        }

        protected void LogError(string message)
        {
            Core.LogError($"[{ExtensionName}] {message}");
        }

        protected void LogException(string message, Exception ex)
        {
            Core.LogException($"[{ExtensionName}] {message}", ex);
        }

        #endregion
    }
}
