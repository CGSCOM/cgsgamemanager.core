﻿using System;
using CGSGameManager.Core.Libraries;
using CGSGameManager.Core.Libraries.WebRequest;

namespace CGSGameManager.Core.Extensions
{
    public sealed class CoreExtension : Extension
    {
        internal CoreExtension(ExtensionManager manager) : base(manager)
        {
        }

        public override string ExtensionName => "Core";

        public override string ExtensionAuthor => "CGS Team";

        public override VersionNumber ExtensionVersion => VersionNumber.CgsCurrent();

        protected override void Load()
        {
            try
            {
                RegisterLibrary(nameof(Translations), new Translations());
                RegisterLibrary(nameof(GameDetails), new GameDetails());
                RegisterLibrary(nameof(WebRequests), new WebRequests());
            }
            catch (Exception e)
            {
                LogException("Unknown Error", e);
            }
        }
    }
}
