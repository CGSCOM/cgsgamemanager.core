﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Libraries;
using CGSGameManager.Core.Plugins;

namespace CGSGameManager.Core.Extensions
{
    public sealed class ExtensionManager
    {
        private const string SearchPattern = "CGSGameManager.*.dll";
        private readonly Hash<string, Extension> _loadedExtensions;
        private bool _init = false;


        public ExtensionManager()
        {
            _loadedExtensions = new Hash<string, Extension>();
            OnPluginRegistered += OnPluginLoaded;
            OnPluginUnregistered += OnPluginRemoved;
            OnLibraryRegistered += OnLibraryLoaded;
            OnLibraryUnregistered += OnLibraryRemoved;
        }

        /// <summary>
        ///     Gets a ThreadSafe count of total loaded Libraries
        /// </summary>
        public int LoadedLibraryCount => AllLoadedLibraries().Count();

        /// <summary>
        ///     Gets a ThreadSafe count of total loaded Plugins
        /// </summary>
        public int LoadedPluginCount => AllLoadedPlugins().Count();

        /// <summary>
        ///     Gets a ThreadSafe count of total loaded Extensions
        /// </summary>
        public int LoadedExtensionCount
        {
            get
            {
                lock (_loadedExtensions)
                {
                    return _loadedExtensions.Count;
                }
            }
        }

        public bool ServerInitComplete
        {
            get => _init;
            private set
            {
                if (_init) return;
                ExtensionInitialize();
                _init = value;
            }
        }


        internal void Shutdown()
        {
            lock (_loadedExtensions)
            {
                foreach (var ext in _loadedExtensions.Values) ext.InternalUnload();

                _loadedExtensions.Clear();
            }
        }

        #region Libraries

        internal static Action<Library> OnLibraryRegistered;
        internal static Action<Library> OnLibraryUnregistered;

        private void OnLibraryLoaded(Library lib)
        {
            if (lib == null)
                return;

            var plugins = AllLoadedPlugins().ToList();

            if (!plugins.Any())
                return;

            foreach (var plugin in plugins)
            {
                var libmembers = Utility.GetMembersFromType(plugin, lib.GetType()).ToList();

                if (!libmembers.Any())
                    continue;

                foreach (var dlib in libmembers) Utility.SetReference(dlib, plugin, lib);
            }
        }

        private void OnLibraryRemoved(Library lib)
        {
            if (lib == null)
                return;

            var plugins = AllLoadedPlugins().ToList();

            if (!plugins.Any())
                return;

            foreach (var plugin in plugins)
            {
                var libmembers = Utility.GetMembersFromType(plugin, lib.GetType()).ToList();

                if (!libmembers.Any())
                    continue;

                foreach (var dlib in libmembers) Utility.SetReference(dlib, plugin, null);
            }
        }

        public T GetLibrary<T>() where T : Library
        {
            return AllLoadedLibraries().FirstOrDefault(l => l is T) as T;
        }

        public Library GetLibrary(string name)
        {
            return string.IsNullOrEmpty(name)
                ? null
                : AllLoadedLibraryNamePairs().FirstOrDefault(a => a.Key.ToLower().Equals(name)).Value;
        }

        private IEnumerable<Library> AllLoadedLibraries()
        {
            lock (_loadedExtensions)
            {
                return _loadedExtensions.Values.SelectMany(a => a.LibraryCollection.Values);
            }
        }

        private IEnumerable<KeyValuePair<string, Library>> AllLoadedLibraryNamePairs()
        {
            lock (_loadedExtensions)
            {
                foreach (var extension in _loadedExtensions.Values)
                    lock (extension.LibraryCollection)
                    {
                        foreach (var libset in extension.LibraryCollection) yield return libset;
                    }
            }
        }

        internal bool IsLibraryNameTaken(string name)
        {
            return !string.IsNullOrEmpty(name) && AllLoadedLibraryNamePairs()
                       .Any(a => string.Equals(a.Key, name, StringComparison.CurrentCultureIgnoreCase));
        }

        #endregion

        #region Plugins

        internal static Action<Plugin> OnPluginRegistered;
        internal static Action<Plugin> OnPluginUnregistered;


        private void OnPluginLoaded(Plugin plugin)
        {
            if (plugin == null)
                return;

            var libmembers = Utility.GetMembersFromType(plugin, typeof(Library)).ToList();
            var libs = AllLoadedLibraries().ToList();

            if (!libmembers.Any() || !libs.Any()) return;
            foreach (var member in libmembers)
            {
                Type type = null;

                switch (member)
                {
                    case FieldInfo field:
                        type = field.FieldType;
                        break;
                    case PropertyInfo property:
                        type = property.PropertyType;
                        break;
                    default:
                        continue;
                }

                Utility.SetReference(member, plugin, libs.FirstOrDefault(l => l.GetType() == type));
            }

            if (ServerInitComplete)
                plugin.Call("OnServerInitialized", null);
        }

        private void OnPluginRemoved(Plugin plugin)
        {
            if (plugin == null)
                return;

            var libmembers = Utility.GetMembersFromType(plugin, typeof(Library));

            foreach (var lib in libmembers) Utility.SetReference(lib, plugin, null);
        }

        private IEnumerable<Plugin> AllLoadedPlugins()
        {
            lock (_loadedExtensions)
            {
                return _loadedExtensions.Values.SelectMany(a => a.PluginCollection.Values);
            }
        }

        private IEnumerable<KeyValuePair<string, Plugin>> AllLoadedPluginNamePairs()
        {
            lock (_loadedExtensions)
            {
                foreach (var extension in _loadedExtensions.Values)
                    lock (extension.PluginCollection)
                    {
                        foreach (var libset in extension.PluginCollection) yield return libset;
                    }
            }
        }

        internal bool IsPluginNameTaken(string name)
        {
            return !string.IsNullOrEmpty(name) && AllLoadedPluginNamePairs()
                       .Any(a => string.Equals(a.Key, name, StringComparison.CurrentCultureIgnoreCase));
        }

        internal object CallHook(string hook, object[] args)
        {
            if (hook == "OnServerInitialized")
                ServerInitComplete = true;

            var plugins = hook.StartsWith("I", false, null)
                ? AllLoadedPlugins().Where(p => p.Extension.IsGameExtension).ToList()
                : AllLoadedPlugins().ToList();
            if (!plugins.Any())
                return null;

            var returnvals = ArrayPool.Get(plugins.Count);

            try
            {
                for (var i = 0; i < plugins.Count; i++)
                {
                    var p = plugins[i];

                    if (!p.IsLoaded)
                        continue;

                    var value = p.Call(hook, args);

                    if (value == null)
                        continue;

                    returnvals[i] = value;
                }

                return !returnvals.Any() ? null : returnvals.FirstOrDefault();
            }
            finally
            {
                ArrayPool.Free(returnvals);
            }
        }

        #endregion

        #region Assembly Loading

        private void ExtensionInitialize()
        {
            var extensions = _loadedExtensions.Values;

            foreach (var ext in extensions)
                ext.ServerInitCompleted();
        }

        internal void LoadAllAssemblies()
        {
            var files = Interface.Core.ExtensionDirectory.GetFiles(SearchPattern, SearchOption.AllDirectories);

            foreach (var a in files)
            {
                if (a.FullName.Contains("CGSGameManager.Core.dll")) continue;
                LoadExtensionFromFileAsync(a.FullName);
            }
        }

        public void LoadExtensionFromFileAsync(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                LogError("Failed to load Extension from empty file name");
                return;
            }

            if (!File.Exists(filename))
            {
                LogError($"Attempted to load extension from file {filename} but it doesn't exist");
                return;
            }

            Interface.Core.QueueAsyncAction(HandleAssemblyFileLoadAsync, filename);
        }

        private void HandleAssemblyFileLoadAsync(object state)
        {
            var filename = state.ToString();
            Assembly assem = null;
            FileStream fs = null;
            try
            {
                using (fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    var b = new byte[fs.Length];
                    fs.Read(b, 0, b.Length);
                    assem = Assembly.Load(b);
                }
            }
            catch (Exception ex)
            {
                switch (ex)
                {
                    case BadImageFormatException _:
                        LogError($"Failed to load file {filename} because it does not implement CLR 2.0 or later");
                        break;
                    case FileLoadException _:
                        LogError($"Failed to load file {filename}");
                        break;
                    default:
                        LogException($"Failed to load filename {filename}", ex);
                        break;
                }
            }
            finally
            {
                fs?.Dispose();
            }

            LoadExtensionFromAssembly(assem);
        }

        public void LoadExtensionFromBytes(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return;

            try
            {
                var assembly = Assembly.Load(bytes);
                LoadExtensionFromAssembly(assembly);
            }
            catch (Exception ex)
            {
                switch (ex)
                {
                    case BadImageFormatException _:
                        LogError(
                            $"Failed to load assembly from byte array because it does not implement CLR 2.0 or later");
                        break;
                    default:
                        LogException($"Failed to load assembly from byte array", ex);
                        break;
                }
            }
        }

        public void LoadExtensionFromAssembly(Assembly assembly)
        {
            if (assembly == null)
                return;

            if (!TryGetExtension(assembly, out var extension))
            {
                LogError(
                    $"Failed to load Extension from Assembly {assembly.GetName().Name} as it does not implement the Extension Class");
                return;
            }

            try
            {
                extension.InternalInit();
                lock (_loadedExtensions)
                {
                    _loadedExtensions[extension.GetType().FullName] = extension;
                }

                LogInfo(
                    $"Loaded Extension {extension.ExtensionName} v{extension.ExtensionVersion} by {extension.ExtensionAuthor}");
                // TODO: Add back auto plugin and library loading
                //Interface.Core.NextFrame(() => Interface.Core.PluginManager.LoadPlugin(assembly));
            }
            catch (Exception e)
            {
                LogException($"Failed to load Extension {extension.ExtensionName}", e);
            }
        }

        private bool TryGetExtension(Assembly assembly, out Extension ex)
        {
            var extensionType = typeof(Extension);
            Type loadType = null;

            try
            {
                var name = assembly.GetName().Name.Trim();
                var exportedTypes = assembly.GetExportedTypes();

                foreach (var t in exportedTypes)
                {
                    if (!extensionType.IsAssignableFrom(t)) continue;

                    loadType = t;
                    break;
                }

                ex = null;
                if (loadType == null) return false;

                ex = Activator.CreateInstance(loadType, this) as Extension;

                if (ex == null || name.IsNullOrWhiteSpace()) return true;
                var gn = GameDetails.Instance?.Game?.Replace(" ", string.Empty);

                if (gn.IsNullOrWhiteSpace()) return true;
                lock (_loadedExtensions)
                {
                    if (name.StartsWith("CGSGameManager.") && name.EndsWith(gn) &&
                        _loadedExtensions.Values.Any(p => !p.IsGameExtension))
                        ex.IsGameExtension = true;
                }

                return true;
            }
            catch (Exception e)
            {
                ex = null;
                switch (e)
                {
                    case TypeLoadException _:
                        LogError(
                            $"Failed to load Extension on Assembly {assembly.GetName().Name} No valid Extension Type");
                        return false;
                    case MissingMethodException _:
                        LogError(
                            $"Failed to load Extension on Assembly {assembly.GetName().Name} No valid constructors found");
                        return false;
                    default:
                        LogException($"Failed to load Extension on Assembly {assembly.GetName().Name}", e);
                        return false;
                }
            }
        }

        #endregion

        #region Logging

        private void LogInfo(string message)
        {
            Interface.Core.LogInfo($"[ExtensionManager] {message}");
        }

        private void LogError(string message)
        {
            Interface.Core.LogError($"[ExtensionManager] {message}");
        }

        private void LogWarning(string message)
        {
            Interface.Core.LogWarning($"[ExtensionManager] {message}");
        }

        private void LogException(string message, Exception ex)
        {
            Interface.Core.LogException($"[ExtensionManager] {message}", ex);
        }

        #endregion
    }
}
