﻿using System;
using System.Linq;
using System.Text;

namespace CGSGameManager.Core
{
    public static class CommandLine
    {
        private static readonly string[] RawCommandLineArgs = Environment.GetCommandLineArgs().Skip(1).ToArray();
        private static readonly char[] Delimiters = {'/', '-', '+'};

        /// <summary>
        ///     Determine if s present switch is available in the command line (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <returns>True if yes | false if no</returns>
        public static bool HasSwitch(string @switch)
        {
            if (string.IsNullOrEmpty(@switch))
                return false;

            var delimiter = HasDelimiter(@switch);

            if (delimiter) @switch = @switch.TrimStart(Delimiters);

            lock (RawCommandLineArgs)
            {
                for (var i = 0; i < RawCommandLineArgs.Length; i++)
                {
                    var s = RawCommandLineArgs[i];

                    if (!HasDelimiter(s)) continue;

                    if (s.TrimStart(Delimiters).Equals(@switch)) return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns the value of a switch if switch doesn't have a value then null is returned (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <param name="default">Default returned value if no value is found</param>
        /// <returns>The value of the switch or null if no value is present</returns>
        public static string GetSwitch(string @switch, string @default = null)
        {
            if (string.IsNullOrEmpty(@switch))
                return @default;

            var index = IndexOfSwitch(@switch);

            if (index == -1)
                return @default;

            lock (RawCommandLineArgs)
            {
                if (RawCommandLineArgs.Length == index)
                    return @default;

                index++;

                var variable = RawCommandLineArgs[index];

                if (HasDelimiter(variable))
                    return @default;

                var sb = new StringBuilder(variable);

                for (var i = index + 1; i < RawCommandLineArgs.Length; i++)
                {
                    var s = RawCommandLineArgs[i];

                    if (HasDelimiter(s))
                        return sb.ToString();

                    sb.Append(" " + s);
                }

                return sb.ToString().Trim();
            }
        }

        /// <summary>
        ///     Returns the value of a switch if switch doesn't have a value then default is returned (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <param name="default">Default returned value if no value is found</param>
        /// <returns>The value of the switch or default if no value is present</returns>
        public static int GetSwitch(string @switch, int @default = -1)
        {
            var value = GetSwitch(@switch, @default.ToString());

            return Convert.ToInt32(value);
        }

        /// <summary>
        ///     Returns the value of a switch if switch doesn't have a value then default is returned (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <param name="default">Default returned value if no value is found</param>
        /// <returns>The value of the switch or default if no value is present</returns>
        public static long GetSwitch(string @switch, long @default = -1)
        {
            var value = GetSwitch(@switch, @default.ToString());

            return Convert.ToInt64(value);
        }

        /// <summary>
        ///     Returns the value of a switch if switch doesn't have a value then default is returned (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <param name="default">Default returned value if no value is found</param>
        /// <returns>The value of the switch or default if no value is present</returns>
        public static bool GetSwitch(string @switch, bool @default = false)
        {
            var value = GetSwitch(@switch, @default.ToString());

            return Convert.ToBoolean(value);
        }

        /// <summary>
        ///     Gets the zero-based index of a switch (Thread-Safe)
        /// </summary>
        /// <param name="switch">Name of the command line switch</param>
        /// <returns>Switch index or -1 if not found</returns>
        public static int IndexOfSwitch(string @switch)
        {
            if (string.IsNullOrEmpty(@switch))
                return -1;

            var delimiter = HasDelimiter(@switch);

            if (delimiter) @switch = @switch.TrimStart(Delimiters);

            lock (RawCommandLineArgs)
            {
                for (var i = 0; i < RawCommandLineArgs.Length; i++)
                {
                    var s = RawCommandLineArgs[i];

                    if (!HasDelimiter(s)) continue;

                    if (s.TrimStart(Delimiters).Equals(@switch)) return i;
                }
            }

            return -1;
        }

        /// <summary>
        ///     Determines if a given string has a Delimiter <see cref="Delimiters" /> (Thread-Safe)
        /// </summary>
        /// <param name="switch">Command line switch</param>
        /// <returns>True if string has delimiter and false if not</returns>
        public static bool HasDelimiter(string @switch)
        {
            if (string.IsNullOrEmpty(@switch))
                return false;

            lock (Delimiters)
            {
                return Delimiters.Contains(@switch[0]);
            }
        }
    }
}
